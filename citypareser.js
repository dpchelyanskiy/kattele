const fs = require('fs');
const each = require('foreach');

const cityData = fs.readFileSync('city.json', 'utf8');

module.exports = getCuties = () => {
    const data = JSON.parse(cityData);
    const city = [];

    each(data, (val, key, obj) => {
        each(obj.areas, (val1, key1, obj1) => {
            for (let i in val1.areas) {
                city.push({
                    parent: 1,
                    name: val1.areas[i].name
                });
            }
        })
    });

    return city
};

