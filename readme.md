To rebuild/build project as image - docker build -t kattele .
Then docker-compose build

On dedicated server - docker build https://Nika_TM@bitbucket.org/Nika_TM/kattele_backend.git

To run project - docker-compose up

Connect to redis - docker exec -it NAME_REDIS redis-cli

if will be problem with postgres
docker volume prune

docker-compose down && docker-compose up --force-recreate
