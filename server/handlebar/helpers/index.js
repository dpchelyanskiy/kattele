const hbs = require('handlebars');
const paginate = require('handlebars-paginate');

hbs.registerHelper("checkPage", (options) => {
    return (options.hash.expected === options.hash.val) ? options.fn(options.data.root) : options.inverse(options.data.root);
});

hbs.registerHelper("navClassToggle", (options) => {
    return (options.hash.expected === options.hash.val) ? 'page-header' : 'page-header bg-blue';
});

hbs.registerHelper("categoryActive", (options) => {
    return (options.hash.expected === options.data.root.activeCategory) ? 'class="active"' : '';
});

hbs.registerHelper('paginate', paginate);

hbs.registerHelper('isTrue', (options) => {
    if (options) {
        return true
    }
});

hbs.registerHelper('noProduct', (options) => {
    return options.error !== undefined;
});

hbs.registerHelper('noWishes', (options) => {
    return options !== undefined;
});

hbs.registerHelper("activeCurrency", (options) => {
    return (options.hash.expected === options.data.root.activeCurrency) ? 'class="active"' : '';
});
// hbs.registerHelper('paginationCount', (count, options) => {
//     console.log(count);
//     console.log(options);
//     return count > 1 ? '' : '';
// });
