const {
    Sequelize,
    client
} = require('../config/db');

const Product = client.define('products', {
    article: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите артикул товара'
            }
        }
    },

    name: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Введите название товара'
            },
        }
    },

    availability: {
        type: Sequelize.DataTypes.STRING,
        value: 'Нет в наличии',
        allowNull: false,
    },

    container: {
        type: Sequelize.DataTypes.STRING,
        value: '',
    },

    purchase_price: {
        type: Sequelize.DataTypes.INTEGER,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите цену закупки товара'
            },
        }
    },

    trade_price: {
        type: Sequelize.DataTypes.INTEGER,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите цену поставщика на товар'
            },
        }
    },

    sell_price: {
        type: Sequelize.DataTypes.INTEGER,
        value: '',
        allowNull: false,
    },

    size: {
        type: Sequelize.DataTypes.ARRAY(Sequelize.DataTypes.SMALLINT),
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите размер товара'
            },
        }
    },

    supplier_city: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    supplier_address: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    supplier_name: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    product_id: {
        type: Sequelize.DataTypes.STRING,
        value: '',
    },

    description: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    mainImage: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Выберите фотографию'
            }
        }
    },

    category: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: true,
    },

    imageCategory: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Ошибка создания папки'
            },
        }
    },

    published: {
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
}, {
    // hooks: {
    //     afterFind: function (attributes, options) {
    //         for (let i in attributes) {
    //             attributes[i].sell_price = 9999
    //         }
    //         // console.log(this);
    //     },
    // },
});

module.exports = {
    Product
};