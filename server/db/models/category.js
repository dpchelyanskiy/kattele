const {
    Sequelize,
    client
} = require('../config/db');

const Category = client.define('categories', {
    name: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Введите ваш почтовый адрес'
            },
        }
    }
});

module.exports = {
    Category
};