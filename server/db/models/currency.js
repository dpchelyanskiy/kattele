const {
    Sequelize,
    client
} = require('../config/db');

const Currency = client.define('currency', {
    type: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите тип валюты'
            },
        },
        unique: {
            args: true,
            msg: 'Валюта должна быть уникальна'
        }
    },

    rate: {
        type: Sequelize.DataTypes.FLOAT,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите курс валюты'
            },
        }
    },

    ru_lang: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите название валюты'
            },
        }
    }
});

// Currency.sync({force: true});

module.exports = {Currency};
