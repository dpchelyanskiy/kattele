const {
    Sequelize,
    client
} = require('../config/db');

const PasswordReset = client.define('passwordReset', {
    _key: {
        type: Sequelize.DataTypes.STRING,
        value: null,
    },

    email: {
        type: Sequelize.DataTypes.STRING,
        value: null,
        unique: true
    }
});

// module.exports = {PasswordReset};