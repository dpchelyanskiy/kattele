const {
    Sequelize,
    client
} = require('../config/db');

const Favorites = client.define('favorites', {
    user: {
        type: Sequelize.DataTypes.STRING,
        value: '',

        validate: {
            notEmpty: true,
        }
    },

    products: {
        type: Sequelize.DataTypes.ARRAY(Sequelize.DataTypes.TEXT),
        value: '',
    }
});

// Favorites.sync({force: true});

module.exports = {Favorites};
