const {
    Sequelize,
    client
} = require('../config/db');

const City = client.define('cities', {
    name: {
        type: Sequelize.DataTypes.STRING,
        value: '',

        validate: {
            notEmpty: true,
        }
    },

    parent: {
        type: Sequelize.DataTypes.INTEGER,
        value: '',

        validate: {
            notEmpty: true,
        }
    }
});

module.exports = City;