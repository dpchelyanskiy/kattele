const {
    Sequelize,
    client
} = require('../config/db');

const Country = client.define('countries', {
    name: {
        type: Sequelize.DataTypes.STRING,
        value: '',

        validate: {
            notEmpty: true,
        }
    }
});

module.exports = Country;