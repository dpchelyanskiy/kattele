const {
    Sequelize,
    client
} = require('../config/db');

const OrderProduct = client.define('orderProducts', {
    orderID: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,


        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите идентификатор заказа'
            }
        }
    },

    article: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите артикул товара'
            },
        }
    },

    productName: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите название товара'
            },
        }
    },

    productID: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите индификатор товара'
            },
        }
    },

    cartID: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите индификатор товара в корзине'
            },
        }
    },

    count: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите колличество товара'
            },
        }
    },

    imageCategory: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    mainImage: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    size: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите размер товара'
            },
        }
    },

    sell_price: {
        type: Sequelize.DataTypes.FLOAT,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите цену продажи товара'
            },
        }
    },


    totalPrice: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите общую сумму товара'
            },
        }
    },

    currency: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите валюту'
            },
        }
    },
});

// OrderProduct.sync({force: true});

module.exports = {
    OrderProduct
};
