const {Sequelize, client} = require('../config/db');

const User = client.define('users', {
    email: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            isEmail: {
                args: true,
                msg: 'Введите корректный адрес электронной почты'
            },
            notEmpty: {
                args: true,
                msg: 'Введите ваш почтовый адрес'
            },
        }
    },

    password: {
        type: Sequelize.DataTypes.STRING(120),
        value: null,
        allowNull: true,
    },

    initials: {
        type: Sequelize.DataTypes.STRING(300),
        value: '',
        allowNull: false,
    },

    phone: {
        type: Sequelize.DataTypes.CHAR(12),
        value: '',
        allowNull: false,
    },

    country: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    city: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,
    },

    branch_number: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        value: null,
    },

    role: {
        type: Sequelize.DataTypes.ARRAY(Sequelize.DataTypes.TEXT),
        allowNull: false,
        value: ['USER'],
    },

    comment: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        value: null,
    },

    token: {
        type: Sequelize.DataTypes.STRING,
        value: '',
    }
});

// User.sync({force: true});

module.exports = {User};
