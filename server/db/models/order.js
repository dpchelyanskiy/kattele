const {
    Sequelize,
    client
} = require('../config/db');

const Order = client.define('orders', {
    orderID: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,


        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите идентификатор заказа'
            }
        }
    },

    userName: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите имя покупателя'
            },
        }
    },

    userEmail: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите почту покупателя'
            },
            isEmail: true
        }
    },

    userPhone: {
        type: Sequelize.DataTypes.STRING,
        value: '',
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите телефон покупателя'
            },
        }
    },

    country: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
    },

    city: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
    },

    branch_number: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
    },

    comment: {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
    },

    totalPrice: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите общую сумму товара'
            },
        }
    },

    currency: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите валюту'
            },
        }
    },

    status: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,

        validate: {
            notEmpty: {
                args: true,
                msg: 'Укажите статус заказа'
            },
        }
    }
});

// Order.sync({force: true});

module.exports = {
    Order
};
