const winston = require('winston');
const path = require('path');

// Configure custom app-wide logger
module.exports = winston.createLogger({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'info-file',
            filename: path.resolve(__dirname, '../server/info.log'),
            level: 'info'
        }),
        new (winston.transports.File)({
            name: 'error-file',
            filename: path.resolve(__dirname, '../server/error.log'),
            level: 'error'
        })
    ]
});
