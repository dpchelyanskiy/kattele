const redis = require('redis');
const client = redis.createClient(process.env.REDIS_HOST);
const RedisStore = require('koa-redis');

client.on('connect', () => {
    console.log(`Connected to redis`);
});

client.on('error', err => {
    console.log(`Error: ${err}`);
});

module.exports = {
    client,
    RedisStore,
    redis
};
