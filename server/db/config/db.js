const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const log = require('./logger');
const {
    DB_HOST,
    DB_TYPE
} = process.env;

// Initialize postgres client
const client = new Sequelize(DB_HOST, {
    host: DB_HOST,
    port: 5432,
    dialect: DB_TYPE,
    operatorsAliases: Op,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

client.authenticate().then(() => {
    log.info('!!! DB Connection has been established successfully. !!!');
}).catch(err => {
    log.error('Unable to connect to the database:', err);
});

// RECREATE MODELS FOR DEV
// client.sync({
//     force: true
// });

module.exports = {
    client,
    Sequelize,
    Op
};