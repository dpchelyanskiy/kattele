const Koa = require('koa');
const views = require('koa-views');
const log = require('./db/config/logger');
const session = require('koa-session');
const passport = require('koa-passport');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const {KOA_CONFIG} = require('./controllers/helpers/constants');
const port = process.env.PORT || 3000;
const serve = require('koa-static');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const handlebarsHelpers = require('./handlebar/helpers');

const app = new Koa();

app.use(views(`${path.resolve(appDir)}/public/views`, {
    map: {
        hbs: 'handlebars',
    },
    options: {
        partials: {
            header: `${path.resolve(appDir)}/public/views/partials/header`,
            footer: `${path.resolve(appDir)}/public/views/partials/footer`,
            newProductsList: `${path.resolve(appDir)}/public/views/partials/product`,
            nav: `${path.resolve(appDir)}/public/views/partials/nav`,
            slider: `${path.resolve(appDir)}/public/views/partials/slider`,
            seocomment: `${path.resolve(appDir)}/public/views/partials/seo-comment`,
            commentslider: `${path.resolve(appDir)}/public/views/partials/comment-slider`,
            sliderTopSalesProduct: `${path.resolve(appDir)}/public/views/partials/slider-product`,
            brands: `${path.resolve(appDir)}/public/views/partials/brands`,
            cart: `${path.resolve(appDir)}/public/views/partials/cart`,
            breadCrumbs: `${path.resolve(appDir)}/public/views/partials/bread-crumbs`,
            catalogFilter: `${path.resolve(appDir)}/public/views/partials/catalog-filter`,
            catalogProduct: `${path.resolve(appDir)}/public/views/partials/catalog-product`,
            searchPageProduct: `${path.resolve(appDir)}/public/views/partials/searchPageProduct`,
        }
    },
    defaultLayout: 'layout',
    extension: 'hbs',
    helpers: handlebarsHelpers
}));

app.use(cors({
    credentials: true
}));

// sessions
app.keys = ['P,A:IS5WX^~`b4V|[5^">6A57+h,Dh'];
app.use(session(KOA_CONFIG, app));

// body parser
app.use(bodyParser({
    enableTypes: ['json', 'form', 'multipart']
}));

// authentication
require('./controllers/sessionController/userSession');
app.use(passport.initialize());
app.use(passport.session());


app.use(async (ctx, next) => {
    if (!ctx.session.cart) {
        ctx.session.currency = 'UAH';
        ctx.session.cart = {products: [], totalPrice: 0, totalQty: 0};
    }
    await next(); // This will pause this function until the endpoint handler has resolved
});

// Error Handler - All uncaught exceptions will percolate up to here
app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        log.error(`Request Error ${ctx.url} - ${err.message}`)
    }
});

app.use(async (ctx, next) => {
    try {
        await next();
        const status = ctx.status || 404;
        if (status === 404) {
            ctx.throw(404)
        }
    } catch (err) {
        ctx.status = err.status || 500;
        if (ctx.status === 404) {
            await ctx.redirect('/404')
        }
    }
});

// Mount routes
app.use(require('./routes/index'));
app.use(serve(`${appDir}/public/`));

// Start the app
app.listen(port, () => {
    log.info(`Server listening at port ${port}`)
});
