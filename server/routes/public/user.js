const Router = require('koa-router');
const router = new Router();
const {ac} = require('../../controllers/helpers/roles');
const Product = require('../../controllers/productController');
const Categories = require('../../controllers/categoriesController');
const FindAndAdd = require('../../controllers/cart/add');
const recountItem = require('../../controllers/cart/counter');
const DeleteFromCart = require('../../controllers/cart/delete');
const Cart = require('../../controllers/cart');
const Favorites = require('../../controllers/favoritesController');
const Order = require('../../controllers/orderController');
const Currency = require('../../controllers/currencyController');
const isEmpty = require('../../controllers/helpers/checkEmprtyObject');

router.get('/', async (ctx) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const {message} = await new Product().publicList(activeCurrency);
    const {topList} = await new Product().topSalesList(activeCurrency);
    const navCategories = await new Categories().categoryList();

    await ctx.render('indexPage', {
        url: 'main',
        authorized: ctx.isAuthenticated(),
        activeCurrency,
        products: message.products,
        topList: topList,
        category: navCategories.message.categories,
        currency: currency.message
    });
});

router.get('login', async (ctx, next) => {
    const {message} = await new Categories().categoryList();
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;

    if (!ctx.isAuthenticated()) {
        await ctx.render('login', {
            authorized: ctx.isAuthenticated(),
            category: message.categories,
            activeCurrency,
            url: 'includeMask',
            currency: currency.message
        });
    } else {
        ctx.redirect('user/cabinet');
    }

    await next();
});

router.get('registration', async (ctx, next) => {
    const {message} = await new Categories().categoryList();
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;

    await ctx.render('registration', {
        authorized: ctx.isAuthenticated(),
        currency: currency.message,
        category: message.categories,
        activeCurrency,
        url: 'includeMask',
    });

    await next();
});

router.get('catalog/:page', async (ctx) => {
    const activeCurrency = ctx.session.currency;
    const {topList} = await new Product().topSalesList(activeCurrency);
    const {message} = await new Categories().categoryList();
    const {page} = ctx.params;
    const productsList = await new Product().publicCategory(page, ctx.query, activeCurrency);
    const currency = await new Currency().listCurrency();

    ctx.status = productsList.status;

    await ctx.render('catalogPage', {
        currency: currency.message,
        activeCurrency,
        authorized: ctx.isAuthenticated(),
        category: message.categories,
        topList: topList,
        goods: productsList.message,
        pagination: {
            page,
            pageCount: productsList.message.pageCount
        }
    });
});

router.get('catalog/:category/:page', async (ctx) => {
    const activeCurrency = ctx.session.currency;
    const currency = await new Currency().listCurrency();
    const {topList} = await new Product().topSalesList();
    const {message} = await new Categories().categoryList();
    const {category, page} = ctx.params;
    const productsList = await new Product().productsByCategory(category, page, ctx.query, activeCurrency);

    await ctx.render('catalogPage', {
        currency: currency.message,
        category: message.categories,
        authorized: ctx.isAuthenticated(),
        activeCategory: category,
        activeCurrency,
        topList: topList,
        goods: productsList.message,
        pagination: {
            page,
            pageCount: productsList.message.pageCount
        }
    });
});

router.get('product/:article', async (ctx) => {
    const activeCurrency = ctx.session.currency;
    const currency = await new Currency().listCurrency();
    const {article} = ctx.params;
    const productByID = await new Product().viewProduct(article, activeCurrency);
    const navCategories = await new Categories().categoryList();
    const {choosedCurrency} = productByID;
    ctx.status = productByID.status;
    await ctx.render('productPage', {
        url: 'includeMask',
        currency: currency.message,
        activeCurrency,
        authorized: ctx.isAuthenticated(),
        category: navCategories.message.categories,
        product: productByID.message,
        productCurrency: choosedCurrency,
    });
});

router.get('user/order', async (ctx, next) => {
    const {message} = await new Categories().categoryList();
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;

    await ctx.render('orderPage', {
        currency: currency.message,
        authorized: ctx.isAuthenticated(),
        user: ctx.req.user,
        activeCurrency,
        category: message.categories,
        url: 'includeMask',
    });

    await next();
});

router.get('user/cabinet', async (ctx, next) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const {message} = await new Categories().categoryList();
    const favoritList = await new Favorites().cabinetListOfFavorites(activeCurrency);

    if (ctx.isAuthenticated()) {
        await ctx.render('privateCabinet', {
            currency: currency.message,
            goods: favoritList.message,
            category: message.categories,
            url: 'privateCabinet',
            activeCurrency,
        });
    } else {
        ctx.redirect('/login');
    }
    await next();
});

router.get('404', async (ctx, next) => {
    const {message} = await new Categories().categoryList();
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;

    await ctx.render('404', {
        currency: currency.message,
        authorized: ctx.isAuthenticated(),
        activeCurrency,
        category: message.categories,
        url: '404',
    });
    await next();
});

// router.post('user/auth', (ctx, next) => {
//     const permission = ac.can(ctx.request.body.role).createAny('product');
//     console.log(permission.granted);
// });

router.post('user/cart', async (ctx, next) => {
    const {id} = ctx.request.body;
    const activeCurrency = ctx.session.currency;
    const {cart, choosedCurrency} = await FindAndAdd(ctx, id, activeCurrency);

    ctx.session.cart = cart;
    ctx.body = {
        choosedCurrency,
        cart: ctx.session.cart,
    };
    ctx.status = 200;
    await next();
});

router.del('user/cart', async (ctx, next) => {
    const {id} = ctx.request.body;
    ctx.session.cart = await DeleteFromCart(ctx, id);
    const activeCurrency = ctx.session.currency;
    ctx.body = {
        activeCurrency,
        cart: ctx.session.cart,
    };
    ctx.status = 200;
    await next();
});

router.get('user/cart', async (ctx, next) => {
    const cart = new Cart(ctx.session.cart);
    const activeCurrency = ctx.session.currency;

    ctx.body = {
        cart: await cart.generateArray(activeCurrency, ctx),
        totalPrice: cart.totalPrice ? cart.totalPrice : 0,
        totalQty: cart.totalQty ? cart.totalQty : 0
    };

    await next();
});

router.post('user/cart-counter', async (ctx, next) => {
    const {id} = ctx.request.body;
    const {Currency} = require('../../db/models/currency');
    const activeCurrency = ctx.session.currency;
    const choosedCurrency = await Currency.findOne({where: {type: activeCurrency}});
    ctx.session.cart = await recountItem(ctx, id, activeCurrency);
    ctx.body = {
        choosedCurrency,
        cart: ctx.session.cart,
    };
    ctx.status = 200;
    await next();
});

router.post('user/wishes', async (ctx, next) => {
    if (ctx.isAuthenticated()) {
        const {id} = ctx.request.body;
        const favor = await new Favorites().addToFavorite(id, ctx);

        ctx.body = {
            favor
        }
    }

    await next();
});

router.post('user/make-order', async (ctx, next) => {
    const {body} = ctx.request;
    const {cart} = ctx.session;
    const {currency} = ctx.session;

    const order = await new Order().makeOrder(body, cart, currency);
    const {message, status} = order;
    if (status === 200) {
        ctx.session.cart = {products: [], totalPrice: 0, totalQty: 0};
    }
    ctx.body = message;
    ctx.status = status;

    await next();
});

router.post('user/fast-order', async (ctx, next) => {
    const {body} = ctx.request;
    const {currency} = ctx.session;

    const fastOrder = await new Order().fastOrder(body, currency);
    const {message, status} = fastOrder;
    if (status === 200) {
        ctx.session.cart = {products: [], totalPrice: 0, totalQty: 0};
    }
    ctx.body = message;
    ctx.status = status;

    await next();
});

router.post('user/change-currency', async (ctx, next) => {
    const {currency} = ctx.request.body;
    ctx.session.currency = currency;

    ctx.body = ctx.session.currency;
    ctx.status = 200;
    await next();
});


router.get('howto', async (ctx) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const navCategories = await new Categories().categoryList();

    await ctx.render('howtoPage', {
        url: 'howto',
        authorized: ctx.isAuthenticated(),
        activeCurrency,
        category: navCategories.message.categories,
        currency: currency.message
    });
});

router.get('dropshipping', async (ctx) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const navCategories = await new Categories().categoryList();

    await ctx.render('dropShippingPage', {
        url: 'dropShippingPage',
        authorized: ctx.isAuthenticated(),
        activeCurrency,
        category: navCategories.message.categories,
        currency: currency.message
    });
});

router.get('contacts', async (ctx) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const navCategories = await new Categories().categoryList();

    await ctx.render('contactsPage', {
        url: 'contactsPage',
        authorized: ctx.isAuthenticated(),
        activeCurrency,
        category: navCategories.message.categories,
        currency: currency.message
    });
});

router.get('password-reset', async (ctx) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const navCategories = await new Categories().categoryList();

    if (!ctx.isAuthenticated()) {
        await ctx.render('password-reset', {
            url: 'password-reset',
            authorized: ctx.isAuthenticated(),
            activeCurrency,
            category: navCategories.message.categories,
            currency: currency.message
        });
    } else {
        ctx.redirect('user/cabinet');
    }
});

router.get('user/edit-user', async (ctx) => {
    const currency = await new Currency().listCurrency();
    const activeCurrency = ctx.session.currency;
    const navCategories = await new Categories().categoryList();

    if (ctx.isAuthenticated()) {
        await ctx.render('edit-user', {
            url: 'includeMask',
            authorized: ctx.isAuthenticated(),
            activeCurrency,
            category: navCategories.message.categories,
            currency: currency.message
        });
    } else {
        ctx.redirect('/login');
    }
});

router.get('catalog-search/:page/:value', async (ctx) => {
    const activeCurrency = ctx.session.currency;
    const {topList} = await new Product().topSalesList(activeCurrency);
    const {message} = await new Categories().categoryList();
    const {page, value} = ctx.params;
    const productsList = await new Product().searchProducts(page, value, activeCurrency);
    const currency = await new Currency().listCurrency();

    ctx.status = productsList.status;

    if (value.length > 0) {
        await ctx.render('search-page', {
            currency: currency.message,
            activeCategory: value,
            activeCurrency,
            authorized: ctx.isAuthenticated(),
            category: message.categories,
            topList: topList,
            goods: productsList.message,
            pagination: {
                page,
                pageCount: productsList.message.pageCount
            }
        });
    } else {
        ctx.redirect('/catalog/1');
    }
});

module.exports = router.routes();
