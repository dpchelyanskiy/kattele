const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport');

require('../../controllers/userController/facebook');

router.get('/login/facebook', passport.authenticate('facebook', {
    scope: 'email'
}), async (ctx, next) => {
    await next();
});

// handle the callback after facebook has authenticated the user
router.get('/login/facebook/callback', passport.authenticate('facebook', {
    successRedirect: 'localhost:3005',
    failureRedirect: '/login'
}));

module.exports = router.routes();
