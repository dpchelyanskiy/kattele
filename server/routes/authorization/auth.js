const Router = require('koa-router');
const router = new Router();
const passport = require('koa-passport');
const user = require('../../controllers/userController');
const responder = require('../../controllers/helpers/userResponder');
const checkAuth = require('../middlewares/isAdmin');
const checkPermission = require('../../controllers/helpers/routePermissionCheck');

router.post('/user/registration', async (ctx, next) => {
    try {
        const newUser = await new user().register(ctx.request.body);
        const {message} = newUser;

        ctx.body = {
            message
        };
    } catch (e) {
        ctx.body = e;
        ctx.status = 400;
    }

    await next();
});

router.post('/user/login', async (ctx, next) => {
    const checkUser = await new user().login(ctx.request.body);
    if (await ctx.isAuthenticated()) {
        ctx.status = 200;
        ctx.body = {
            status: '200',
            message: 'Вы авторизованны'
        };

        return false
    }

    if (checkUser.status !== 404) {
        await passport.authenticate('local', (err, user, info, status) => {
            if (err) {
                ctx.status = 403;
                ctx.body = {
                    status: 403,
                    message: 'Пользователь не существует'
                };
            } else {
                // const permission = ac.getGrants(); //Uncomment this - If client will need routes by user role
                ctx.login(user);
                ctx.status = 200;
                ctx.body = {
                    status: 200,
                    message: `Привет, ${user.initials}`,
                };
            }
        })(ctx);

    } else {
        const {status, message} = checkUser;
        ctx.body = {
            status,
            message,
        };
        ctx.status = 200;
    }
    await next();
});

router.post('/user/check/auth', async (ctx, next) => {
    ctx.body = {
        'authorized': await ctx.isAuthenticated(),
    };

    await next();
});

router.post('/user/logout', async (ctx) => {
    if (ctx.isAuthenticated()) {
        ctx.logout();
        ctx.body = {}
    } else {
        ctx.body = {
            message: 'Вы не авторизованны'
        };
        ctx.status = 401;
    }
});

router.post('/user/password-recovery', checkAuth, async (ctx, next) => {
    if (checkPermission(ctx)) {
        const currentUser = ctx.req.user.email;
        responder(await new user(ctx.request.body).recoveryPwd(currentUser), ctx);
    }
    await next();
});

router.post('/user/password-reset', async (ctx, next) => {
    responder(await new user().resetPwd(ctx.request.body), ctx);

    await next();
});

module.exports = router.routes();
