module.exports = checkAuth = async (ctx, next) => {
    if (!ctx.isAuthenticated()) {
        ctx.status = 403;
        ctx.body = {
            status: 403,
            message: 'Вы не авторизованы'
        };

        return false;
    }

    await next();
};
