const Router = require('koa-router');
const router = new Router();

router.use(require('./authorization/auth'));
router.use(require('./authorization/facebook'));
router.use(require('./public/index'));
router.use(require('./admin/index'));
router.use('/user', require('./privateCabinet'));

module.exports = router.routes();
