const Router = require('koa-router');
const router = new Router();
const Currency = require('../../controllers/currencyController/index');
const checkAuth = require('../middlewares/isAdmin');
const responder = require('../../controllers/helpers/userResponder');
const {checkCurrencyPermission} = require('../../controllers/helpers/routePermissionCheck');

router.get('/currency/', checkAuth, checkCurrencyPermission, async (ctx, next) => {
    responder(await new Currency().listCurrency(), ctx);
    await next();
}).del('/currency/', checkAuth, checkCurrencyPermission, async (ctx, next) => {
    const {currencyID} = ctx.request.body;
    responder(await new Currency().deleteCurrency(currencyID), ctx);

    await next();
}).get('/currency/:currencyID', checkAuth, checkCurrencyPermission, async (ctx, next) => {
    const {currencyID} = ctx.params;
    responder(await new Currency().getCurrencyById(currencyID), ctx);
    await next();
}).post('/currency/', checkAuth, checkCurrencyPermission, async (ctx, next) => {
    const {body} = ctx.request;
    responder(await new Currency().addCurrency(body), ctx);

    await next();
}).put('/currency/', checkAuth, checkCurrencyPermission, async (ctx, next) => {
    const {body} = ctx.request;
    responder(await new Currency().updateCurrency(body), ctx);

    await next();
});

module.exports = router.routes();
