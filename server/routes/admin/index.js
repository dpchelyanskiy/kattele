const Router = require('koa-router');
const router = new Router();

//ADMIN ROUTES INDEX
router.use('/api', require('./product'));
router.use('/api', require('./user'));
router.use('/api', require('./csv'));
router.use('/api', require('./category'));
router.use('/api', require('./order'));
router.use('/api', require('./currency'));
// router.use('/user', require('./auth'));

module.exports = router.routes();
