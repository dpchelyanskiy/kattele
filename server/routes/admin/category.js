const Router = require('koa-router');
const router = new Router();
const Category = require('../../controllers/categoriesController/index');
const checkAuth = require('../middlewares/isAdmin');
const responder = require('../../controllers/helpers/userResponder');
const {checkCategoryPermission} = require('../../controllers/helpers/routePermissionCheck');

router.post('/category/', checkAuth, checkCategoryPermission, async (ctx, next) => {
    const data = ctx.request.body;
    responder(await new Category(ctx).makeNewCatgory(data), ctx);
    await next();
}).delete('/category/', checkAuth, checkCategoryPermission, async (ctx, next) => {
    responder(await new Users(ctx).delete(), ctx);

    await next();
});

module.exports = router.routes();
