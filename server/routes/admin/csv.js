const Router = require('koa-router');
const router = new Router();
const checkAuth = require('../middlewares/isAdmin');
const uploadCSV = require('../../controllers/CSVparser/index');
const parse = require('await-busboy');
const fs = require('fs');
const path = require('path');
const responder = require('../../controllers/helpers/userResponder');
const {checkCSVPermission} = require('../../controllers/helpers/routePermissionCheck');
const generateCSV = require('../../controllers/generateCSV');
const generateOrderCSV = require('../../controllers/generateCSV/order');

router.post('/upload/csv', checkAuth, checkCSVPermission, async (ctx, next) => {
    const parts = parse(ctx, {
        checkFile: (fieldname, file, filename) => {
            if (path.extname(filename) !== '.csv') {
                const err = new Error('Выбранный файл не является форматом .CSV');
                err.status = 400;
                return err
            }
        }
    });

    const pts = await parts;
    if (pts === undefined) {
        ctx.body = {
            message: 'Выберите файл CSV'
        };

        await next();
    } else {
        const fname = pts.filename.replace(/\s/g, '').toLowerCase();
        const file = `${path.resolve(`server/csvfiles/${fname}`)}`;
        const makeFile = fs.createWriteStream(file);
        pts.pipe(await makeFile);

        const products = await uploadCSV(file);

        try {
            ctx.body = products;
            await next();
        } catch (e) {
            ctx.body = e;
            ctx.status = 400;
            await next();
        }
    }
}).get('/download/csv', checkAuth, checkCSVPermission, async (ctx, next) => {
    responder(await generateCSV(ctx.request.body), ctx);

    await next();
});


router.get('/download/order-csv', checkAuth, checkCSVPermission, async (ctx, next) => {
    responder(await generateOrderCSV(ctx.request.body), ctx);

    await next();
});
module.exports = router.routes();



