const Router = require('koa-router');
const router = new Router();
const checkAuth = require('../middlewares/isAdmin');
const Product = require('../../controllers/productController');
const responder = require('../../controllers/helpers/userResponder');
const {checkProductsPermission} = require('../../controllers/helpers/routePermissionCheck');

router.post('/products/', checkAuth, checkProductsPermission, async (ctx, next) => {
    responder(await new Product().createProduct(ctx), ctx);
    await next();
}).get('/products/:page/:limit', checkAuth, checkProductsPermission, async (ctx, next) => {
    const {page, limit} = ctx.params;
    responder(await new Product().list(page, limit), ctx);

    await next();
}).get('/products/:article', checkAuth, checkProductsPermission, async (ctx, next) => {
    const {article} = ctx.params;
    responder(await new Product().viewProduct(article), ctx);

    await next();
}).put('/products/', checkAuth, checkProductsPermission, async (ctx, next) => {
    responder(await new Product().updateProduct(ctx), ctx);

    await next();
}).delete('/products/:id', checkAuth, checkProductsPermission, async (ctx, next) => {
    const {id} = ctx.params;
    responder(await new Product().deleteProduct(id), ctx);

    await next();
}).post('/products/bulk/removal/', checkAuth, checkProductsPermission, async (ctx, next) => {
    const data = ctx.request.body;
    responder(await new Product().bulkDelete(data), ctx);

    await next();
});

module.exports = router.routes();
