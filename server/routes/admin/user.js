const Router = require('koa-router');
const router = new Router();
const Users = require('../../controllers/userController/index');
const checkAuth = require('../middlewares/isAdmin');
const responder = require('../../controllers/helpers/userResponder');
const {checkUserControllerPermission} = require('../../controllers/helpers/routePermissionCheck');

router.get('/users/:page/:limit', checkAuth, checkUserControllerPermission, async (ctx, next) => {
    responder(await new Users(ctx).list(), ctx);
    await next();
}).get('/users/:id', checkAuth, checkUserControllerPermission, async (ctx, next) => {
    responder(await new Users(ctx).findUser(), ctx);
    await next();
}).put('/users/', checkAuth, checkUserControllerPermission, async (ctx, next) => {
    responder(await new Users(ctx).updateUser(), ctx);

    await next();
}).delete('/users/', checkAuth, checkUserControllerPermission, async (ctx, next) => {
    responder(await new Users(ctx).delete(), ctx);

    await next();
});

module.exports = router.routes();
