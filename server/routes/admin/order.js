const Router = require('koa-router');
const router = new Router();
const Order = require('../../controllers/orderController/index');
const checkAuth = require('../middlewares/isAdmin');
const responder = require('../../controllers/helpers/userResponder');
const {checkOrderPermission} = require('../../controllers/helpers/routePermissionCheck');

router.get('/order/:page/:limit', checkAuth, checkOrderPermission, async (ctx, next) => {
    const {page, limit} = ctx.params;
    responder(await new Order().listOrders(page, limit), ctx);
    await next();
}).del('/order/:orderID', checkAuth, checkOrderPermission, async (ctx, next) => {
    const {orderID} = ctx.params;
    responder(await new Order().deleteOrder(orderID), ctx);

    await next();
}).post('/order/', checkAuth, checkOrderPermission, async (ctx, next) => {
    const {orderID} = ctx.request.body;
    responder(await new Order().getOrderById(orderID), ctx);

    await next();
}).put('/order/', checkAuth, checkOrderPermission, async (ctx, next) => {
    const {body} = ctx.request;
    responder(await new Order().updateOrder(body), ctx);

    await next();
});

module.exports = router.routes();
