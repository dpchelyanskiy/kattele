const Router = require('koa-router');
const router = new Router();
const checkUserAuth = require('../middlewares/userCheck');
const PrivateCabinet = require('../../controllers/privateCabinetController');
const responder = require('../../controllers/helpers/userResponder');

router.get('/private/:id', checkUserAuth, async (ctx, next) => {
    responder(await new PrivateCabinet().private(ctx), ctx);

    await next();
}).put('/private', checkUserAuth, async (ctx, next) => {
    responder(await new PrivateCabinet().privateUpdate(ctx), ctx);
    await next();
});

router.get('/private/favorites', async (ctx, next) => {
    responder(await new PrivateCabinet().privateUpdate(ctx), ctx);

    await next();
});


module.exports = router.routes();
