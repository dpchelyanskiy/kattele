const nodemailer = require('nodemailer');
const {EMAIL_LOGIN, EMAIL_PASS, EMAIL_HOST} = process.env;

let transporter = nodemailer.createTransport({
    // pool: true,
    host: EMAIL_HOST,
    port: 465,
    secure: true, // use TLS
    auth: {
        user: EMAIL_LOGIN,
        pass: EMAIL_PASS
    },
    tls: {
        rejectUnauthorized: false
    }
});

const sendMail = (userEmail, action) => {
    const {title, message} = action;
    let mailOptions = {
        from: '"KATTELE 👻" <support@kattele.com>', // sender address
        to: userEmail, // list of receivers
        subject: title, // Subject line
        text: message,
        html: message, // html body
        dkim: {
            domainName: 'kattele.com',
            keySelector: 'default._domainkey',
            privateKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDGBef0db1y53Rx1kMcyd3s4jII0Qr2nbQ5OIru8yamhew+mYlhB5Ilq7BT/2KUbvie36fucG3Kwk23AGveYBMi88lQxxsqj4JYI29rSxLx1ytZlCdTiAx/65GRTqDMiUg1fraS0Q4ndurwBcTrdQa7YxsJzCnPXiEfzlLDmaLgHQIDAQAB'
        }
    };

    return transporter.sendMail(mailOptions);
};


module.exports = {
    sendMail
};
