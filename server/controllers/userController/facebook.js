const passport = require('koa-passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const {User} = require('../../db/models/user');
const {FB_APPID, FB_APP_SECRET, FB_CALLBACK_URL} = process.env;
const userContoller = require('../../controllers/userController');

passport.use(new FacebookStrategy({
        clientID: FB_APPID,
        clientSecret: FB_APP_SECRET,
        callbackURL: FB_CALLBACK_URL,
        profileFields: ['id', 'displayName', 'emails', 'photos']
    },
    // facebook will send back the tokens and profile
    (access_token, refresh_token, profile, done) => {
        const {emails} = profile;
        process.nextTick(() => {
            // find the user in the database based on their facebook id
            User.findOne({
                where: {
                    email: emails[0].value
                }
            }).then(async (user, err) => {
                if (err)
                    return done(err);
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    try {
                        profile.token = access_token;
                        const newUser = await new userContoller(profile).register();
                        return done(null, newUser.user);
                    } catch (e) {
                        console.log(e);
                    }
                }
            });
        });
    })
);
