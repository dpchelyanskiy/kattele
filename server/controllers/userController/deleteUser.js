const {User} = require('../../db/models/user');

module.exports = deleteUser = (data) => {
    const {email} = data.request.body;

    return User.findOne({
        where: {
            email
        }
    }).then((user) => {
        if (!user) {
            return {
                message: 'Пользователь не существует',
                status: 400
            };
        }

        return User.destroy({
            where: {
                email
            }
        }).then(() => {
            return {
                message: {
                    success: `Пользователь ${user.initials} удален`,
                    _id: user.id
                },
                status: 200
            }
        }).catch((err) => {
            return {
                message: err,
                status: 400,
            };
        });
    })
};
