const {User} = require('../../db/models/user');
const serializeData = require('../helpers/serialize');
const {ac} = require('../../controllers/helpers/roles');
const passGen = require('uniqid');
const {getPasshash} = require('../../controllers/helpers/passcrypt');

module.exports = updateUser = async (data) => {
    const userData = await serializeData(data.request.body);
    const {id} = data.req.user;

    const canUpdateRole = ac.can().role(data.req.user.role).updateAny('role').granted;
    if (!canUpdateRole) userData.role = ['user']; else {
        userData.role = data.req.user.role
    }

    const {email, initials, country, city, branch_number, role, comment, repassword} = userData;
    let {password, phone} = userData;
    phone = phone.replace(/\s/g, '');
    let setNewPass = '';
    if (password !== '' && password === repassword) {
        const newPass = passGen.time();
        setNewPass = getPasshash(newPass);
    } else if (password !== '' && password !== repassword) {
        return {
            message: {
                status: 'error',
                msg: 'Ваши пароли не совпадают',
            },
            status: 200
        };
    }

    if (id) {
        return User.findById(id).then((user) => {
            if (user) {
                return user.update({
                    email, initials, phone, country, city, branch_number, role, comment,
                }).then((updated) => {
                    if (setNewPass !== '') {
                        user.updateAttributes({
                            password: setNewPass
                        });
                    }
                    return {
                        message: {
                            status: 200,
                            msg: `Пользователь ${updated.initials} обновлен`,
                        },
                        status: 200
                    };
                }).catch(err => {
                    return {
                        message: {
                            status: 200,
                            msg: err,
                        },
                        status: 300
                    };
                });
            } else {
                return {
                    message: 'Нет такого пользователя',
                    status: 400
                }
            }
        })
    } else {
        return {
            message: 'Ошибка данных пользователя',
            status: 400
        }
    }
};
