const {User} = require('../../db/models/user');

module.exports = findUserById = (id) => {
    return User.find({
        attributes: {exclude: ['password', 'createdAt', 'updatedAt', 'token']},
        where: {
            id
        }
    }).then((user) => {
        if (user) {
            return {
                message: user,
                status: 200
            }
        }

        return {
            message: 'Такого пользователя нет',
            status: 400
        }
    }).catch((err) => {
        return {
            message: err,
            status: 500
        }
    })
};
