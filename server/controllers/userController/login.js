const {User} = require('../../db/models/user');
const {checkPass} = require('../../controllers/helpers/passcrypt');

module.exports = login = (user) => {
    const {email, password} = user;

    return User.findOne({
        where: {
            email
        }
    }).then((user, err) => {
        if (!user) {
            return {
                message: `Пользователь не существует`,
                status: 404
            };
        }

        if (!checkPass(password, user.password)) {
            return {
                message: `Неправильный пароль`,
                status: 404
            };
        }

        return true;
    }).catch((error) => {
        return {
            message: error,
            status: 404
        };
    })
};
