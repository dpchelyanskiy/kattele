const {User} = require('../../db/models/user');

module.exports = userList = (req) => {
    const {page, limit} = req.params;

    return User.findAndCountAll().then(result => {
        let pageCount = Math.ceil(result.count / limit),
            offset = (page * limit) - limit;

        return User.findAll({
            order: [['id', 'DESC']],
            offset: offset < 0 ? 0 : offset,
            limit: limit,
            attributes: {exclude: ['password', 'createdAt', 'updatedAt', 'token']}
        }).then(users => {
            return {
                message: {
                    users,
                    pageCount
                },
                status: 200
            }
        }).catch((err) => {
            return err
        })
    })
};
