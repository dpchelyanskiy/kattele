const {User} = require('../../db/models/user');
const {checkPass, getPasshash} = require('../../controllers/helpers/passcrypt');

module.exports = passwordRecovery = (data, currentUser) => {
    const {email, password, oldpass} = data;
    if (currentUser !== email) {
        return {
            message: {
                error: 'У вас нет прав на данное действие'
            },
            status: 403,
        }
    }

    return User.find({
        where: {
            email
        }
    }).then((user) => {
        const canChangePass = checkPass(oldpass, user.password);

        if (canChangePass) {
            const newPass = getPasshash(password);
            return user.updateAttributes({
                password: newPass
            }).then((updated) => {
                return {
                    message: {
                        updated: email,
                        status: "Пароль обновлен"
                    },
                    status: 200
                }
            }).catch((error => {
                return error
            }))
        }

        return {
            message: {
                error: "Пароли не совпадают"
            },
            status: 403
        }
    }).catch((error) => {
        return {
            message: {
                error
            },
            status: 400
        }
    });
};
