const registration = require('./registration');
const deleteUser = require('./deleteUser');
const userList = require('./userList');
const findUserById = require('./userById');
const updateUser = require('./updateUser');
const resetPassword = require('./resetPassword');
const passwordRecovery = require('./passwordRecovery');
const loginn = require('./login');

module.exports = class User {
    constructor(body) {
        this.body = body;
        this.id = body ? body.params.id : '';
    }

    register(data) {
        return registration(data);
    }

    login(user) {
        return login(user)
    }

    delete() {
        return deleteUser(this.body);
    }

    list() {
        return userList(this.body);
    }

    findUser() {
        return findUserById(this.id)
    }

    updateUser() {
        return updateUser(this.body)
    }

    resetPwd(data) {
        return resetPassword(data)
    }

    recoveryPwd(currentUser) {
        return passwordRecovery(this.body, currentUser);
    }
};
