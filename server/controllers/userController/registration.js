const {User} = require('../../db/models/user');
const {SERVER_ERRORS} = require('../../controllers/helpers/constants');
const {getPasshash} = require('../../controllers/helpers/passcrypt');
const serializeData = require('../helpers/serialize');

const registration = body => {
    body.phone = body.phone.replace(/[.*+?^${}()\s-|[\]\\]/g, "");
    if (body.provider) {
        body = {
            email: body.emails[0].value,
            initials: body.displayName,
            password: '',
            phone: '',
            country: '',
            city: '',
            branch_number: '',
            role: ['user'],
            token: body.token,
            provider: body.provider
        };
    }

    const newUser = serializeData(body);

    const {email, initials, phone, country, city, branch_number, role = ['user'], comment, token} = newUser;

    const {status} = SERVER_ERRORS.RESTRICTED;

    return User.findOne({
        where: {
            email: email
        }
    }).then(async (exist) => {
        if (exist) {
            return {
                status,
                message: {
                    status: 'error',
                    msg: 'Такой пользователь существует'
                }
            }
        } else {
            let {password} = body;
            if (body.provider === undefined && password === '') {
                return Promise.reject({
                    status: 401,
                    msg: 'Введите пароль'
                })
            } else if (body.provider === undefined && initials === '') {
                return Promise.reject({
                    status: 401,
                    msg: 'Введите ваши инициалы'
                });
            }

            body.provider ? password = '' : password = getPasshash(password);

            return User.create({
                email,
                password,
                initials,
                phone,
                country,
                city,
                branch_number,
                role: ['user'],
                comment,
                token
            }, {
                replacements: ['<script>']
            }).then((user) => {
                return {
                    user,
                    status: 200,
                    message: 'Пользователь зарегистрирован'
                };
            }).catch((err) => {
                return Promise.reject(err)
            })
        }
    }).catch((err) => {
        return Promise.reject(err)
    });
};

module.exports = registration;
