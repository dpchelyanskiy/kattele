const {User} = require('../../db/models/user');
const {sendMail} = require('../../controllers/mailer');
const {RESET_PASSWORD} = require('../../controllers/mailer/templates');
// const {PasswordReset} = require('../../db/models/passwordReset');
const passGen = require('uniqid');
const {getPasshash} = require('../../controllers/helpers/passcrypt');


//TODO RESET PASSWORD BY URL
// const createResetStatus = (email) => {
//     const _key = keyGenerator.time('resetID_');
//     return PasswordReset.create({email, _key}).then((created) => {
//         return created._key;
//     }).catch((err) => {
//         return err
//     });
// };
// const deleteResetStatus = (email, exist) => {
//     if (exist) {
//         return exist.destroy().then(() => {
//             return checkEmailResetStatus(email);
//         }).catch((err) => {
//             return err
//         })
//     }
//     checkEmailResetStatus(email);
// };
// const checkEmailResetStatus = (email) => {
//     return PasswordReset.findOne({
//         where: {
//             email
//         }
//     }).then(async (exist) => {
//         if (!exist) {
//             return {
//                 message: await createResetStatus(email),
//             };
//         }
//         return {
//             message: await deleteResetStatus(email, exist),
//         };
//     }).catch((err) => {
//         console.log(err);
//         return err;
//     });
// };

module.exports = resetPassword = (data) => {
    const {email} = data;

    return User.findOne({
        where: {
            email
        }
    }).then(async (user, err) => {
        if (!user) {
            return {
                message: {
                    status: 'error',
                    msg: 'Такой пользователь не существует',
                },
                status: 200
            }
        }

        //TODO RESETPASSWORD BY URL
        // console.log(await checkEmailResetStatus(email));
        // if (checkEmailResetStatus(email)) {
        // }

        const newPass = passGen.time();
        const hash = getPasshash(newPass);
        user.updateAttributes({
            password: hash
        });

        RESET_PASSWORD.message = `<div>Это эл. письмо для сброса пароля с сайта <b>http://kattele.com</b></div> <br/><br/> <div><b>Ваш новый пароль - ${newPass}</b></div>`;
        return sendMail(email, RESET_PASSWORD).then((info) => {
            return {
                message: {
                    sentTo: info.accepted[0]
                },
                status: 200
            }
        }).catch((err) => {
            return {
                message: {
                    rejected: err.rejected[0]
                },
                status: 400
            }
        });
    }).catch((err) => {
        return {
            message: err,
            status: 400
        }
    });
};
