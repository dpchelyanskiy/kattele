const {Favorites} = require('../../db/models/favorites');
const {Product} = require('../../db/models/products');
module.exports = (productID, ctx) => {
    const {email} = ctx.req.user;

    return Product.findById(productID).then((product) => {
        if (product) {
            return Favorites.findOrCreate({
                where: {
                    user: email,
                },
                defaults: {
                    products: [productID]
                }
            }).spread((exists, created) => {
                if (!created && !exists.products.includes(productID)) {
                    exists.update({
                        products: [...exists.products, productID]
                    });
                } else if (!created && exists.products.includes(productID)) {
                    const updatedArray = exists.products.filter(function (number) {
                        return number !== productID;
                    });

                    exists.update({
                        products: updatedArray
                    });
                }

                return {
                    message: "success",
                    status: 200
                }
            });
        }

        return {
            message: "В базе нет такого товара",
            status: 404
        }
    }).catch((err) => {
        return {
            message: err,
            status: 400
        }
    });
};
