const addToFavorite = require('./addToFavorite');
const listOfFavorites = require('./listOfFavorites');
const cabinetListOfFavorites = require('./cabinetListOfFavorites');

module.exports = class Favorites {
    addToFavorite(productID, ctx) {
        return addToFavorite(productID, ctx);
    }

    listOfFavorites(activeCurrency) {
        return listOfFavorites(activeCurrency);
    }

    cabinetListOfFavorites(activeCurrency) {
        return cabinetListOfFavorites(activeCurrency);
    }
};
