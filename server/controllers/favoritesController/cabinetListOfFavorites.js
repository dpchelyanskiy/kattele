const {Favorites} = require('../../db/models/favorites');
const {Product} = require('../../db/models/products');
const {Currency} = require('../../db/models/currency');
const fs = require('fs');
const path = require('path');
const appDir = path.dirname(require.main.filename);

module.exports = (activeCurrency) => {
    return Favorites.findAll().then((favorites) => {
        return Product.findAll({
            where: {
                id: {in: favorites[0].products}
            },
            /* where: {
                 category: {
                     [client.Op.not]: null
                 },
                 published: true
             },*/
            order: [['id', 'DESC']],
        }).then(async (products) => {
            if (products.length === 0) {
                return {
                    status: 200,
                    message: {
                        error: 'Нет избранных товаров :(',
                        status: 200
                    }
                }
            }

            const choosedCurrency = await Currency.findOne({where: {type: activeCurrency}});
            
            for (let i in products) {
                if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                    products[i].dataValues.sell_price /= choosedCurrency.rate;
                    products[i].dataValues.sell_price = products[i].dataValues.sell_price.toFixed(2);
                }

                products[i].favorite = true;
                if (fs.existsSync(`${appDir}/public/images/products/${products[i].imageCategory})`)) {
                    const dir = path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`);
                    const files = fs.readdirSync(dir);

                    products[i].dataValues.allImages = files.filter((image) => image !== products[i].dataValues.image);
                } else {
                    products[i].dataValues.allImages = ['Нет фотографий для слайдера']
                }
            }

            return {
                status: 200,
                message: {
                    products,
                    choosedCurrency
                }
            }
        }).catch((err) => {
            return {
                status: 301,
                message: err
            }
        })
    }).catch((err) => {
        return err
    });
};
