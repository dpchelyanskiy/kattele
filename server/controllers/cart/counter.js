const Cart = require('./index');
const {Currency} = require('../../db/models/currency');

module.exports = recountItem = async (ctx, productID, activeCurrency) => {
    const cart = new Cart(ctx.session.cart);
    const {body} = ctx.request;

    cart.minus(body, productID, activeCurrency);
    return cart;
};

