const {Currency} = require('../../db/models/currency');

module.exports = function Cart(oldCart) {
    this.items = oldCart.items || {};
    this.itemsCount = oldCart.itemsCount || 0;
    this.totalQty = Number(oldCart.totalQty) || 0;
    this.totalPrice = Number(oldCart.totalPrice) || 0;


    this.add = (item) => {
        let storedItem = this.items[item.cartID];
        if (!storedItem) {
            storedItem = this.items[item.cartID] = {product: item, qty: Number(item.count), item_total_price: 0};
        } else {
            storedItem.product.count += item.count;
            storedItem.total_price = Number(storedItem.product.sell_price) * storedItem.product.count;
        }

        storedItem.item_total_price = Number(storedItem.product.sell_price) * storedItem.product.count;

        this.totalPrice = 0;
        for (let i in this.items) {
            this.totalPrice += this.items[i].item_total_price;
        }
    };


    this.delete = (id) => {
        let storedItem = this.items[id];
        if (storedItem === undefined) {
            return {products: [], totalPrice: 0, totalQty: 0}
        } else {
            this.totalPrice -= storedItem.item_total_price;
            delete this.items[id];
        }
    };

    this.minus = async (item, id) => {
        if (item.action === 'minus' && this.items[id].product.count !== 1) {
            this.items[id].product.count--;
            this.items[id].item_total_price = this.items[id].product.sell_price * this.items[id].product.count;
            this.items[id].item_total_price = Number(this.items[id].item_total_price.toFixed(2));

            this.totalPrice = 0;
            for (let i in this.items) {
                this.totalPrice += this.items[i].item_total_price;
            }
        } else {
            this.items[id].product.count++;
            this.items[id].item_total_price = this.items[id].product.sell_price * this.items[id].product.count;
            this.items[id].item_total_price = Number(this.items[id].item_total_price.toFixed(2));

            this.totalPrice = 0;
            for (let i in this.items) {
                this.totalPrice += this.items[i].item_total_price;
            }
        }
    };

    this.generateArray = async (activeCurrency, ctx) => {
        // console.log(ctx.session.cart);
        let arr = [];
        const choosedCurrency = await Currency.findOne({where: {type: activeCurrency}});
        this.totalPrice = 0;
        for (let id in this.items) {
            console.log('asdsaddsd', ctx.session.cart.items[id]);
            if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                this.items[id].product.sell_price = this.items[id].product.oldSell_price / choosedCurrency.rate;
                this.items[id].product.sell_price = this.items[id].product.sell_price.toFixed(2);
                this.items[id].item_total_price = this.items[id].product.sell_price * this.items[id].product.count;
                this.items[id].item_total_price = this.items[id].product.count > 1 ? Math.ceil(this.items[id].item_total_price) : this.items[id].item_total_price;
                ctx.session.cart.items[id].item_total_price = this.items[id].product.count > 1 ? Math.ceil(this.items[id].item_total_price) : this.items[id].item_total_price
                ctx.session.cart.totalPrice = this.items[id].product.count > 1 ? Math.ceil(this.items[id].item_total_price) : this.items[id].item_total_price
            } else {
                this.items[id].product.sell_price = this.items[id].product.oldSell_price;
                this.items[id].item_total_price = this.items[id].product.sell_price * this.items[id].product.count;
                this.items[id].item_total_price = this.items[id].product.sell_price * this.items[id].product.count;
                this.items[id].item_total_price = Math.ceil(this.items[id].item_total_price);
                ctx.session.cart.items[id].item_total_price = this.items[id].product.sell_price * this.items[id].product.count;
                ctx.session.cart.totalPrice = this.items[id].product.sell_price * this.items[id].product.count;
            }
            this.totalPrice += this.items[id].item_total_price;
            console.log(this.totalPrice);
            // console.log(this.items[id]);
            arr.push(this.items[id]);
        }
        return {
            products: arr.sort(function (a, b) {
                return a - b
            }),
            products_length: arr.length,
            choosedCurrency
        };
    }
};
