const {Product} = require('../../db/models/products');
const {Currency} = require('../../db/models/currency');

const Cart = require('./index');

module.exports = FindAndAdd = async (ctx, productID, activeCurrency) => {
    const cart = new Cart(ctx.session.cart ? ctx.session.cart : {items: []});
    const {body} = ctx.request;
    return Product.find({
        where: {
            id: productID
        },
        attributes: ['id', 'article', 'name', 'trade_price', 'sell_price', 'size', 'mainImage', 'imageCategory']
    }).then(async (product, err) => {
        if (err) {
            return ctx.send(err);
        }
        const choosedCurrency = await Currency.findOne({where: {type: activeCurrency}});

        product.dataValues.oldSell_price = product.dataValues.sell_price;
        product.dataValues.oldTrade_price = product.dataValues.trade_price;

        if (choosedCurrency && choosedCurrency.type !== 'UAH') {
            product.dataValues.sell_price /= choosedCurrency.rate;
            product.dataValues.sell_price = product.dataValues.sell_price.toFixed(2);
            product.dataValues.trade_price /= choosedCurrency.rate;
            product.dataValues.trade_price = product.dataValues.trade_price.toFixed(2);
        }

        product.dataValues.size = Number(body.size);
        product.dataValues.count = Number(body.count);
        product.dataValues.cartID = `${product.id}_${body.size}`;

        cart.add(product.dataValues, product.id);
        return {
            cart,
            choosedCurrency
        };
    }).catch((err) => {
        return {
            message: err,
            status: 400
        }
    })
};

