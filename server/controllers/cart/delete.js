const Cart = require('./index');

module.exports = DeleteFromCart = (ctx, productID) => {
    const cart = new Cart(ctx.session.cart);

    cart.delete(productID);
    return cart;
};

