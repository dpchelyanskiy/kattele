const Cart = require('./index');

module.exports = function GetCartProducts(req, res) {
    const cart = new Cart(req.session.cart);
    res.send({
        products: cart.generateArray(),
        totalPrice: cart.totalPrice ? cart.totalPrice : 0,
        totalQty: cart.totalQty ? cart.totalQty : 0
    })
};
