const download = require('image-downloader');
const fs = require('fs');
const uniqid = require('uniqid');
const path = require('path');
const im = require('imagemagick');

const imageParser = async (url, dest) => {
    const options = {
        url,
        dest
    };

    try {
        return await resizeAndSave(download.image(options), dest)
    } catch (e) {
        console.error(e);
    }
};

const resizeAndSave = async (file, dest) => {
    const {filename} = await file;
    const fileType = filename.split(".").pop(),
        newName = path.resolve(`${dest}/product-${uniqid.time()}.${fileType}`);

    im.identify(filename, (err, features) => {
        if (err) throw err;
        const height = Number(features.height - 180);
        const width = Number(features.width);

        if (fs.readFileSync(filename)) {
            im.crop({
                srcPath: filename,
                dstPath: newName,
                width,
                height,
                quality: 1,
                gravity: "Center"
            }, function (err, stdout, stderr) {
                if (!err) {
                    return fs.unlink(`${filename}`, (err) => {
                        if (err) console.log('Error in image rebuild', err);
                        console.log(`${filename} was deleted`);
                    });
                } else {
                    console.error('Ошибка сборки фотографии');
                }
            });
        }
    });

    return {
        file: newName.split("/").pop(),
        destination: dest.split("/").pop()
    }
};


module.exports = downloadIMG = async (data) => {
    const dir = path.resolve(`${process.cwd()}/public/images/products/product-${uniqid.time()}`);
    await fs.mkdirSync(dir);

    return await imageParser(data.mainImage, dir);
};
