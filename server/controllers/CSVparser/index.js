const fs = require('fs');
const csv = require('fast-csv');
const {dataBrute} = require('./dataBrute');
const Product = require('../../controllers/productController');

module.exports = uploadCSV = async (file) => {
    const csvData = [];

    return new Promise(async (resolve, reject) => {
        setTimeout(() => {
            try {
                fs.accessSync(file, fs.constants.R_OK | fs.constants.W_OK);
                const stream = fs.createReadStream(file);
                const csvStream = csv({
                    delimiter: ';',
                    headers: true,
                    renameHeaders: false,
                    objectMode: true,
                    columns: true,
                    skip_empty_lines: true,
                    trim: true
                }).on("data", function (data) {
                    csvData.push(data);
                }).on("end", async () => {
                    const sendResponse = async () => {
                        const data = {
                            existProducts: [],
                            createdProducts: []
                        };
                        for (let i of csvData) {
                            const recievedData = await new Product().bulkCSVCreate(await dataBrute(i));
                            if (recievedData._article) {
                                data.existProducts.push(recievedData._article);
                            } else {
                                data.createdProducts.push(recievedData);
                            }
                        }
                        resolve(data);
                    };

                    fs.unlink(file, await sendResponse);
                }).on("error", (err) => {
                    reject(err);
                });

                stream.pipe(csvStream);
            } catch (e) {
                reject({
                    message: 'Ошибка сервера, попробуйте позже',
                })
            }
        }, 1000);
    });
};
