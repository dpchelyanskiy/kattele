const zagruzkaTemplate = {
    "Категория 1": "supplier_city",
    "Категория 2": "container",
    "Категория 3": "supplier_name",
    "Категория 4": "",
    "Артикул": "article",
    "Заголовок": "name",
    "Цена": "purchase_price",
    "Наличие": "availability",
    "Описание": "description",
    "Изображение 1": "mainImage",
    "Вес": "",
    "Габариты": "",
    "Производитель": "",
    "Ссылка на товар": "",
    "ID": "product_id",
    "Размер": "size"
};


module.exports = {
    zagruzkaTemplate
};
