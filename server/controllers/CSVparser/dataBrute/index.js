const {zagruzkaTemplate} = require('../templates/zag_csv');

const dataBrute = async (data) => {
    let bData = {};

    for (let i in data) {
        if (zagruzkaTemplate[i]) {
            bData[zagruzkaTemplate[i]] = data[i];
        }
    }

    return handlingData(bData)
};

const handlingData = async (data) => {
    let {size, description, purchase_price} = data;

    data.purchase_price = Number(purchase_price.split(',')[0]);
    data.description = description.replace(/<[^>]*>/g, ' ');
    data.trade_price = 0;
    data.sell_price = 0;
    data.supplier_address = '';

    data.size = await makeProductSize(data.size, size);

    return data
};

const makeProductSize = (existSize, size) => {
    existSize = [];
    let newSize = size.split(';');

    for (let i of newSize) {
        if (i.indexOf('-') > 0) {
            const sizeIN = i.split('-');
            existSize.push(sizeIN[0], sizeIN[1]);
            // for (let y = sizeIN[0]; y <= sizeIN[1]; y++) {
            //
            //     console.log(y);
            // }
        } else {
            existSize.push(i);
        }
    }


    return existSize
};

module.exports = {
    dataBrute
};
