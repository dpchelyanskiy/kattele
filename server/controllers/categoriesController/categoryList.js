const {Category} = require('../../db/models/category');

module.exports = categoryList = () => {
    return Category.findAll({
        order: [['id', 'DESC']],
    }).then((categories) => {
        if (!categories) {
            return categories
        }
        return {
            status: 200,
            message: {
                categories,
            }
        }
    }).catch((err) => {

    })
};
