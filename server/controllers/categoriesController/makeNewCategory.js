const {Category} = require('../../db/models/category');

module.exports = makeNewCategory = (data) => {
    return Category.create(data).then((res) => {
        return {
            message: res.name,
            status: 200
        };
    }).catch((err) => {
        return err;
    });
};
