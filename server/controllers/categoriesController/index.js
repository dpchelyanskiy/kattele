const makeNewCategory = require('./makeNewCategory');
const deleteCategory = require('./deleteCategory');
const categoryList = require('./categoryList');

module.exports = class Categories {
    makeNewCatgory(data) {
        return makeNewCategory(data);
    }

    deleteCategory() {
        return deleteCategory();
    }

    categoryList() {
        return categoryList();
    }
};
