const serializeData = (data) => {
    const userData = typeof data !== "object" ? JSON.parse(data) : data;

    let newData;
    for (let i in userData) {
        if (typeof userData[i] !== 'object') {
            newData = ({...newData, [i]: userData[i].toString().replace(/[&\-/\\#+()$~%'":*?<>{}]/g, "")});
        } else {
            newData = ({...newData, [i]: userData[i]});
        }
    }

    return newData
};

module.exports = serializeData;
