const AccessControl = require('accesscontrol');

let grantsObject = {
    admin: {
        product: {
            'delete:any': ['*'],
        },
        users: {
            'update:any': ['*'],
            'delete:any': ['*'],
            'read:any': ['*'],
        },

        category: {
            'create:any': ['*'],
            'delete:any': ['*'],
            'read:any': ['*'],
        }
    },

    manager: {
        isAdmin: {
            'read:any': ['*'],
        },

        account: {
            'read:any': ['*']
        },

        product: {
            'read:any': ['*'],
            'create:any': ['*'],
            'update:any': ['*'],
        },

        order: {
            'read:any': ['*'],
            'create:any': ['*'],
            'update:any': ['*'],
            'delete:any': ['*'],
        },

        currency: {
            'read:any': ['*'],
            'create:any': ['*'],
            'update:any': ['*'],
            'delete:any': ['*'],
        },

        users: {
            'read:any': ['*'],
        },

        role: {
            'update:any': ['*'],
        },

        csv: {
            'create:any': ['*'],
            'read:any': ['*']
        },
    },

    user: {
        account: {
            'create:own': ['*'],
        },
        product: {
            'read:any': ['*'],
        },
        users: {
            'update:own': ['*'],
        },

        currency: {
            'read:any': ['*']
        }
    },
};
const ac = new AccessControl(grantsObject);

ac.grant('admin').extend(['user', 'manager']);

module.exports = {
    ac
};
