const {client, RedisStore} = require('../../db/config/cache');

const SERVER_ERRORS = {
    RESTRICTED: {
        message: {
            error: 'Доступ запрещен',
            status: 403
        },
        status: 403,
    },

    METHODS: {
        message: {
            error: 'Нет такого метода',
            status: 400
        },
        status: 400
    }
};

const KOA_CONFIG = {
    key: 'usr:ss', /** (string) cookie key (default is koa:sess) */
    /** (number || 'session') maxAge in ms (default is 1 days) */
    /** 'session' will result in a cookie that expires when session/browser is closed */
    /** Warning: If a session cookie is stolen, this cookie will never expire */
    maxAge: 86400000,
    autoCommit: true,
    /** (boolean) automatically commit headers (default true) */
    overwrite: true,
    /** (boolean) can overwrite or not (default true) */
    httpOnly: true,
    /** (boolean) httpOnly or not (default true) */
    signed: true,
    /** (boolean) signed or not (default true) */
    rolling: false,
    /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: true,
    /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: new RedisStore({
        client,
        db: 1
    })
};

module.exports = {
    KOA_CONFIG,
    SERVER_ERRORS,
    client
};
