module.exports = responder = (data, ctx) => {
    const {message, status} = data;

    ctx.body = message;
    ctx.status = status;
};
