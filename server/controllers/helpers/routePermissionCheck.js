const {ac} = require('../../controllers/helpers/roles');
const {SERVER_ERRORS} = require('../../../server/controllers/helpers/constants');
const responder = require('../../controllers/helpers/userResponder');

const checkProductsPermission = async (ctx, next) => {
    const role = ctx.req.user.role;
    const roleCan = {
        GET: 'readAny',
        PUT: 'updateAny',
        POST: 'createAny',
        DELETE: 'deleteAny'
    };

    const userMethod = roleCan[ctx.request.method];

    if (userMethod === undefined) {
        responder(SERVER_ERRORS.METHODS, ctx);
        return false;
    }

    if (!ac.can().role(role)[userMethod]('product').granted) {
        responder(SERVER_ERRORS.RESTRICTED, ctx);
        return false;
    }

    await next();
};

const checkCSVPermission = async (ctx, next) => {
    const role = ctx.req.user.role;
    const roleCan = {
        GET: 'readAny',
        POST: 'createAny',
    };

    const userMethod = roleCan[ctx.request.method];

    if (userMethod === undefined) {
        responder(SERVER_ERRORS.METHODS, ctx);
        return false;
    }

    if (!ac.can().role(role)[userMethod]('csv').granted) {
        responder(SERVER_ERRORS.RESTRICTED, ctx);
        return false;
    }

    await next();
};

const checkUserControllerPermission = async (ctx, next) => {
    const role = ctx.req.user.role;

    let roleCan = {
        GET: 'readAny',
        PUT: 'updateAny',
        POST: 'createAny',
        DELETE: 'deleteAny'
    };

    if (role[0] === 'user') {
        roleCan.PUT = 'updateOwn';
    }

    const userMethod = roleCan[ctx.request.method];

    if (userMethod === undefined) {
        responder(SERVER_ERRORS.METHODS, ctx);
        return false;
    }

    if (!ac.can().role(role)[userMethod]('users').granted) {
        responder(SERVER_ERRORS.RESTRICTED, ctx);
        return false;
    }

    await next();
};

const checkCategoryPermission = async (ctx, next) => {
    const role = ctx.req.user.role;
    const roleCan = {
        GET: 'readAny',
        PUT: 'updateAny',
        POST: 'createAny',
        DELETE: 'deleteAny'
    };

    const userMethod = roleCan[ctx.request.method];

    if (userMethod === undefined) {
        responder(SERVER_ERRORS.METHODS, ctx);
        return false;
    }

    if (!ac.can().role(role)[userMethod]('category').granted) {
        responder(SERVER_ERRORS.RESTRICTED, ctx);
        return false;
    }

    await next();
};

const checkOrderPermission = async (ctx, next) => {
    const role = ctx.req.user.role;
    const roleCan = {
        GET: 'readAny',
        PUT: 'updateAny',
        POST: 'createAny',
        DELETE: 'deleteAny'
    };

    const userMethod = roleCan[ctx.request.method];

    if (userMethod === undefined) {
        responder(SERVER_ERRORS.METHODS, ctx);
        return false;
    }

    if (!ac.can().role(role)[userMethod]('order').granted) {
        responder(SERVER_ERRORS.RESTRICTED, ctx);
        return false;
    }

    await next();
};

const checkCurrencyPermission = async (ctx, next) => {
    const role = ctx.req.user.role;
    const roleCan = {
        GET: 'readAny',
        PUT: 'updateAny',
        POST: 'createAny',
        DELETE: 'deleteAny'
    };

    const userMethod = roleCan[ctx.request.method];

    if (userMethod === undefined) {
        responder(SERVER_ERRORS.METHODS, ctx);
        return false;
    }

    if (!ac.can().role(role)[userMethod]('currency').granted) {
        responder(SERVER_ERRORS.RESTRICTED, ctx);
        return false;
    }

    await next();
};

module.exports = {
    checkProductsPermission,
    checkCSVPermission,
    checkUserControllerPermission,
    checkCategoryPermission,
    checkOrderPermission,
    checkCurrencyPermission
};
