const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);

const getPasshash = (pass) => {
    return bcrypt.hashSync(pass, salt); // generate pass hash when register
};

const checkPass = (pass, hash) => {
    return bcrypt.compareSync(pass, hash); // check pass hash when login
};

module.exports = {
    getPasshash,
    checkPass
};
