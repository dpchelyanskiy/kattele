const {Currency} = require('../../db/models/currency');

module.exports = (id) => {
    return Currency.findById(id, {
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        }
    }).then(currency => {
        return {
            message: currency,
            stack: 200
        }
    }).catch(err => {
        return {
            message: err,
            stack: 200
        }
    })
};
