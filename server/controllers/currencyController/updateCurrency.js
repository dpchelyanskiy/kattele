const {Currency} = require('../../db/models/currency');

module.exports = (data) => {
    const {id} = data;
    console.log(data);

    return Currency.findById(id).then(currency => {
        if (currency) {
            currency.update(data);

            return {
                message: `Валюта ${currency.type} обновлен`,
                status: 200,
            }
        }

        return {
            message: 'Нет такой валюты',
            status: 200,
        }
    }).catch(err => {
        return {
            message: err,
            status: 200,
        }
    })
};
