const {Currency} = require('../../db/models/currency');

module.exports = currencyID => {
    return Currency.findById(currencyID).then(currency => {
        if (currency) {
            currency.destroy();

            return {
                message: `Валюта ${currency.type} удалена`,
                status: 200
            }
        }

        return {
            message: 'Такой валюты не существует',
            status: 200
        }
    }).catch(err => {
        return {
            message: err,
            status: 200
        }
    });
};
