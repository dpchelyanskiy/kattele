const addCurrency = require('./addCurrency');
const listCurrency = require('./listCurrency');
const deleteCurrency = require('./deleteCurrency');
const getCurrencyById = require('./getCurrencyById');
const updateCurrency = require('./updateCurrency');

module.exports = class Currency {
    addCurrency(data) {
        return addCurrency(data);
    };

    listCurrency() {
        return listCurrency();
    };

    deleteCurrency(currencyID) {
        return deleteCurrency(currencyID);
    };

    getCurrencyById(currencyID) {
        return getCurrencyById(currencyID);
    };

    updateCurrency(body) {
        return updateCurrency(body)
    };
};
