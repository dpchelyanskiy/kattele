const {Currency} = require('../../db/models/currency');

module.exports = () => {
    return Currency.findAll({
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        }
    }).then(currencies => {
        if (currencies.length > 0) {
            return {
                message: currencies,
                status: 200
            }
        }

        return {
            message: 'В базе нет валют',
            status: 200
        }
    })
};
