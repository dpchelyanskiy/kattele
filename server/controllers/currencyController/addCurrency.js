const {Currency} = require('../../db/models/currency');

module.exports = data => {
    return Currency.create(data).then(currency => {
        return {
            message: `Валюта ${currency.type} создана`,
            status: 200
        }
    }).catch(err => {
        return {
            message: err,
            status: 200
        }
    });
};
