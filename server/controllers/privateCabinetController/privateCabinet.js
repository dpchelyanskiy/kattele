const {User} = require('../../db/models/user');

module.exports = privateCabinet = (ctx) => {
    const {id} = ctx.req.user,
        requestedUser = ctx.params.id;

    if (Number(id) !== Number(requestedUser)) {
        return {
            message: 'not found',
            status: 404
        }
    }

    return User.find({
        where: {
            id
        },
        attributes: {
            exclude: ['password', 'createdAt', 'updatedAt', 'token', 'role', 'comment']
        }
    }).then((user) => {
        return {
            message: user,
            status: 200
        }
    }).catch((error => {
        return {
            message: error,
            status: 400
        }
    }))
};
