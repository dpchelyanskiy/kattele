const privateCabinet = require('./privateCabinet');
const User = require('../../controllers/userController/index');

module.exports = class PrivateCabinet extends User {
    constructor() {
        super();
    };

    private(ctx) {
        return privateCabinet(ctx);
    };

    privateUpdate(ctx) {
        this.body = ctx;
        return super.updateUser()
    };

    favorites() {

    }
};
