const {Order} = require('../../db/models/order');

module.exports = (page, limit) => {
    return Order.findAndCountAll().then((products, err) => {
        let pageCount = Math.ceil(products.count / limit),
            offset = (page * limit) - limit,
            productsCount = products.count;

        if (!err) {
            return Order.findAll({
                order: [['id', 'DESC']],
                offset: offset < 0 ? 0 : offset,
                limit: limit,
                attributes: {exclude: ['createdAt', 'updatedAt']}
            }).then(async (orders) => {
                if (orders.length === 0) {
                    return {
                        status: 200,
                        message: {
                            error: 'Заказы не найденны',
                            status: 200
                        }
                    }
                }

                return {
                    status: 200,
                    message: {
                        orders,
                        pageCount,
                        productsCount
                    }
                }
            }).catch((err) => {
                return {
                    status: 301,
                    message: err
                }
            })
        } else {
            return {
                status: 301,
                message: err
            }
        }
    });
};
