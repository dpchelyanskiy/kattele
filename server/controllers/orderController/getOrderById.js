const {Order} = require('../../db/models/order');
const {OrderProduct} = require('../../db/models/orderProduct');

module.exports = orderID => {
    return Order.findOne({
        where: {
            orderID
        }
    }).then(order => {
        if (order) {
            return OrderProduct.findAll({
                where: {
                    orderID
                }
            }).then(products => {
                return {
                    message: {
                        orderInfo: order,
                        products
                    },
                    status: 200
                };
            }).catch(err => {
                return {
                    message: err,
                    status: 200
                }
            })
        }

        return {
            message: 'Нет такого заказа',
            status: 200
        }
    }).catch(err => {
        return {
            message: err,
            status: 200
        }
    });
};
