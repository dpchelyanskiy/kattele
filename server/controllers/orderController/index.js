const makeOrder = require('./makeOrder');
const listOrders = require('./listOrders');
const deleteOrder = require('./deleteOrder');
const getOrderById = require('./getOrderById');
const updateOrder = require('./updateOrder');
const fastOrder = require('./fastOrder');

module.exports = class Order {
    makeOrder(data, cart, currency) {
        return makeOrder(data, cart, currency);
    };

    listOrders(page, limit) {
        return listOrders(page, limit);
    };

    deleteOrder(orderID) {
        return deleteOrder(orderID);
    };

    getOrderById(orderID) {
        return getOrderById(orderID);
    };

    updateOrder(body) {
        return updateOrder(body)
    };

    fastOrder(body, currency) {
        return fastOrder(body, currency);
    }
};
