const {Order} = require('../../db/models/order');
const {OrderProduct} = require('../../db/models/orderProduct');

module.exports = (data) => {
    const {orderID} = data;
    if (!orderID) {
        return {
            message: 'Индиификатор пуст',
            status: 200
        }
    }
    return Order.findOne({
        where: {
            orderID
        }
    }).then(order => {
        const {deletedProducts, userName, userEmail, userPhone, country, city, branch_number, comment} = data;
        let {totalPrice} = order;

        if (order && deletedProducts.length > 0) {
            OrderProduct.findAll({
                where: {
                    orderID,
                    cartID: deletedProducts
                }
            }).then(products => {
                for (let i in products) {
                    products[i].destroy();
                    totalPrice = totalPrice - products[i].totalPrice;
                }

                order.update({
                    userName, userEmail, userPhone, country, city, branch_number, comment, totalPrice
                });
            });

            return {
                message: `Заказа № - ${orderID} обновлен`,
                status: 200
            }
        }

        return {
            message: 'В базе нет такого заказа',
            status: 200
        }
    }).catch(err => {
        return {
            message: err,
            status: 200
        }
    });
};
