const {Order} = require('../../db/models/order');
const {OrderProduct} = require('../../db/models/orderProduct');

module.exports = orderID => {
    return Order.findOne({
        where: {
            orderID
        }
    }).then(order => {
        if (order) {
            order.destroy();
            OrderProduct.findAll({
                where: {
                    orderID
                }
            }).then(products => {
                for (let i in products) {
                    products[i].destroy();
                }
            }).catch(err => {
                return {
                    message: err,
                    status: 200
                }
            });

            return {
                message: `Заказ № - ${order.orderID} удален`,
                status: 200
            }
        }

        return {
            message: 'Нет такого заказа',
            status: 200
        }
    }).catch(err => {
        return {
            message: err,
            status: 400
        }
    })
};
