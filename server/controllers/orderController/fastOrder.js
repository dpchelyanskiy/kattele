const {Order} = require('../../db/models/order');
const {OrderProduct} = require('../../db/models/orderProduct');
const {Currency} = require('../../db/models/currency');
const {Product} = require('../../db/models/products');
const uniqid = require('uniqid');

module.exports = (data, currency) => {
    const {productID, productCount, productSize, phone, email, comment} = data;
    const orderID = uniqid.time();

    return Product.findById(productID).then(async product => {
        if (product) {
            const choosedCurrency = await Currency.findOne({where: {type: currency}});
            if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                product.sell_price /= choosedCurrency.rate;
                product.sell_price = product.sell_price.toFixed(2);
            }

            const totalPrice = productCount * product.sell_price;

            const orderInfo = {
                userName: 'Быстрый заказ',
                orderID,
                userEmail: email,
                userPhone: phone,
                comment,
                totalPrice,
                currency,
                status: 'Новый',
            };

            const cartID = `${product.id}_${productSize}`;
            const fastOrderProduct = {
                orderID,
                article: product.article,
                productName: product.name,
                productID: product.id,
                cartID,
                count: productCount,
                imageCategory: product.imageCategory,
                mainImage: product.mainImage,
                size: productSize,
                sell_price: product.sell_price,
                totalPrice,
                currency,
            };

            Order.create(orderInfo);
            OrderProduct.create(fastOrderProduct);

            return {
                message: `Спасибо, ваш заказ принят`,
                status: 200
            }
        }

        return {
            message: 'Нет такого товара',
            status: 200
        }
    }).catch(err => {
        console.log(err);
    });
};
