const {Order} = require('../../db/models/order');
const {OrderProduct} = require('../../db/models/orderProduct');
const uniqid = require('uniqid');

module.exports = (data, cart, currency) => {

    console.log(cart.items);
    if (cart.items === undefined) {
        return {
            message: 'Нет товаров для оформления заказа',
            status: 200
        }
    }
    const orderedProducts = [];

    const orderID = uniqid.time();
    const {name, country, phone, email, city, branch_number, comment} = data;

    const orderInfo = {
        orderID,
        userName: name,
        userEmail: email,
        userPhone: phone,
        country,
        city,
        branch_number,
        comment,
        totalPrice: cart.totalPrice,
        currency,
        status: 'Новый',
    };

    for (let i in cart.items) {
        orderedProducts.push({
            orderID,
            article: cart.items[i].product.article,
            productName: cart.items[i].product.name,
            productID: cart.items[i].product.id,
            cartID: cart.items[i].product.cartID,
            count: cart.items[i].product.count,
            imageCategory: cart.items[i].product.imageCategory,
            mainImage: cart.items[i].product.mainImage,
            size: cart.items[i].product.size,
            sell_price: cart.items[i].product.sell_price,
            totalPrice: cart.items[i].item_total_price,
            currency,
        });
    }

    return Order.create(orderInfo).then((order) => {
        if (order) {
            return OrderProduct.bulkCreate(orderedProducts).then((products) => {
                if (products) {
                    return {
                        message: 'Заказ оформлен',
                        status: 200
                    }
                }
            }).catch((err) => {
                return {
                    message: err,
                    status: 400
                }
            })
        }
    }).catch((err) => {
        return {
            message: err,
            status: 400
        }
    })
};
