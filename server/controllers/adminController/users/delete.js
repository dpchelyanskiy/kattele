const {User} = require('../../../db/models/user');

module.exports = removeUser = (id) => {
    User.findById(id).then((user) => {
        user.destroy()
    })
};
