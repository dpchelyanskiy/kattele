const removeUser = require('./delete');

module.exports = class Admin {
    constructor(email, id) {
        this.id = id;
        this.email = email;
    }

    delete() {
        return removeUser(this.id);
    }
};
