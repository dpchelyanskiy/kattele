const passport = require('koa-passport');
const {User} = require('../../db/models/user');
const LocalStrategy = require('passport-local').Strategy;
const options = {
    usernameField: 'email',
    passwordField: 'password'
};

const {checkPass} = require('../../controllers/helpers/passcrypt');

passport.serializeUser((user, done) => {
    done(null, !!user && user.id);
});

passport.deserializeUser((id, done) => {
    return User.findById(id).then((user) => {
        done(null, user);
    }).catch((err) => {
        done(err, null);
    });
});

passport.use(new LocalStrategy(options, (email, password, done) => {
    User.find({
        where: {
            email
        }
    }).then((user) => {
        if (!user) return done(null, false);
        const passIsCorrect = checkPass(password, user.password);

        if (passIsCorrect) {
            return done(null, user);
        } else {
            return done(null, false);
        }
    }).catch((err) => {
        return done(err);
    });
}));
