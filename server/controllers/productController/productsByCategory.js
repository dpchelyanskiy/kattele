const {Product} = require('../../db/models/products');
const {Op} = require('../../db/config/db');
const {Currency} = require('../../db/models/currency');
const fs = require('fs');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const Favorites = require('../../controllers/favoritesController');

module.exports = productsByCategory = (categoryName, page, query, activeCurrency) => {
    const {size, price, sortBy} = query;
    let querySize;
    if (size !== undefined) {
        querySize = size.split(',');
    }

    let conditionSize = {
        where: {
            category: categoryName
        }
    };
    const sortByConst = {
        'новинки': [
            'createdAt', 'ASC'
        ],
        'от дешевых к дорогим': [
            'sell_price', 'ASC'
        ],

        'от дорогих к дешевым': [
            'sell_price', 'DESC'
        ]
    };

    conditionSize = {
        order: [['createdAt', 'DESC']],
        where: {
            ...conditionSize.where,
        },
    };

    querySize !== undefined ? conditionSize = {
        where: {
            ...conditionSize.where,
            size: {
                [Op.overlap]: querySize
            }
        },
    } : '';

    price !== undefined ? conditionSize = {
        where: {
            ...conditionSize.where,
            sell_price: {
                [Op.lte]: price
            }
        },
    } : '';

    sortBy !== undefined ? conditionSize = {
        order: [sortByConst[sortBy]],
        where: {
            ...conditionSize.where,
        },
    } : '';

    const limit = 20;
    let pageCount = 0;
    let offset = (page * limit) - limit;
    return Product.findAndCountAll(conditionSize
        // {
        //     where: {
        //         category: {
        //             [client.Op.not]: null
        //         },
        //         published: true
        //     },
        // }
    ).then((products, err) => {
        let productsCount = products.count;

        productsCount > limit ? pageCount = Math.ceil(productsCount / limit) : '';

        conditionSize = {
            ...conditionSize,
            offset: offset < 0 ? 0 : offset,
            limit: limit,
            where: {
                ...conditionSize.where,
            },
        };
        if (!err) {
            return Product.findAll(conditionSize).then(async (products) => {
                products.length <= limit ? pageCount = Math.ceil(productsCount / limit) : '';

                if (products.length === 0) {
                    return {
                        status: 200,
                        message: {
                            error: 'Товары не найдены :(',
                            status: 200
                        }
                    }
                }

                const favorList = await new Favorites().listOfFavorites();
                let favorListProducts = [];

                if(favorList.length) {
                    favorListProducts = favorList[0].dataValues.products;
                }

                const choosedCurrency = await Currency.findOne({where: {type: activeCurrency,}});

                for (let i in products) {
                    if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                        products[i].dataValues.sell_price /= choosedCurrency.rate;
                        products[i].dataValues.sell_price = products[i].dataValues.sell_price.toFixed(2);
                    }
                    for (let y in favorListProducts) {
                        if (Number(favorListProducts[y]) === (Number(products[i].id))) {
                            products[i].favorite = true;
                        }
                    }

                    // products[i].sell_price = products[i].sell_price * 2;
                    if (fs.existsSync(`${appDir}/public/images/products/${products[i].imageCategory})`)) {
                        const dir = path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`);
                        const files = fs.readdirSync(dir);

                        products[i].dataValues.allImages = files.filter((image) => image !== products[i].dataValues.image);
                    } else {
                        products[i].dataValues.allImages = ['Нет фотографий для слайдера']
                    }
                }

                return {
                    status: 200,
                    message: {
                        products,
                        pageCount,
                        productsCount,
                        productCurrency: choosedCurrency
                    }
                }
            }).catch((err) => {
                return {
                    status: 301,
                    message: err
                }
            })
        } else {
            return {
                status: 301,
                message: err
            }
        }
    });
};
