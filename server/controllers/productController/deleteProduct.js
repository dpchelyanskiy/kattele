const {Product} = require('../../db/models/products');
const path = require('path');
const rimraf = require('rimraf');
const appDir = path.dirname(require.main.filename);
const fs = require('fs');

module.exports = deleteProduct = (id) => {
    return Product.findById(id).then((product, err) => {
        if (product) {
            return Product.destroy({
                where: {
                    article: product.article
                }
            }).then(() => {
                if (fs.existsSync(path.resolve(`${appDir}/public/images/products/${product.imageCategory}`))) {
                    const dir = path.resolve(`${appDir}/public/images/products/${product.imageCategory}`);
                    rimraf(dir, () => {
                        console.log(`${product.imageCategory} deleted`);
                    });
                }

                return {
                    message: `Товар ( ${product.name} ) удален`,
                    status: 200
                }
            })
        }

        if (err) {
            return {
                message: err,
                status: 404
            }
        }

        return {
            message: 'Продукт не найден',
            status: 404
        }
    })
};
