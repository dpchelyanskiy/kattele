const {Product} = require('../../db/models/products');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const fs = require('fs');

module.exports = getProductsList = (page, limit) => {
    return Product.findAndCountAll().then((products, err) => {
        let pageCount = Math.ceil(products.count / limit),
            offset = (page * limit) - limit,
            productsCount = products.count;

        if (!err) {
            return Product.findAll({
                order: [['id', 'DESC']],
                offset: offset < 0 ? 0 : offset,
                limit: limit
            }).then(async (products) => {
                if (products.length === 0) {
                    return {
                        status: 200,
                        message: {
                            error: 'В категории нет товаров, пожалуйста выберите другую категорию товаров',
                            status: 200
                        }
                    }
                }

                for (let i in products) {
                    if (fs.existsSync(`${appDir}/public/images/products/${products[i].imageCategory})`)) {
                        const dir = path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`);
                        const files = fs.readdirSync(dir);

                        products[i].dataValues.allImages = files.filter((image) => image !== products[i].dataValues.image);
                    } else {
                        products[i].dataValues.allImages = ['no images']
                    }
                }

                return {
                    status: 200,
                    message: {
                        products,
                        pageCount,
                        productsCount
                    }
                }
            }).catch((err) => {
                return {
                    status: 301,
                    message: err
                }
            })
        } else {
            return {
                status: 301,
                message: err
            }
        }
    });
};
