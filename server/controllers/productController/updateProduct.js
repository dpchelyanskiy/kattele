const {Product} = require('../../db/models/products');
const asyncBusboy = require('async-busboy');
const fs = require('fs');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const uniqid = require('uniqid');

module.exports = updateProduct = async (ctx) => {
    if (ctx.request.is('multipart/form-data')) {
        const {fields, files} = await asyncBusboy(ctx.req);
        const {data, deletedImages} = fields;
        let dataToUpdate = JSON.parse(data);
        const {id, imageCategory, mainImage} = dataToUpdate;


        const dir = path.resolve(`${appDir}/public/images/products/${imageCategory}`);
        const existDir = await fs.existsSync(dir);

        if (!existDir) {
            fs.mkdirSync(dir);
            dataToUpdate.imageCategory = dir.split("products/").pop();
        }

        if (deletedImages !== undefined) {
            const images = JSON.parse(deletedImages);
            const dir = path.resolve(`${appDir}/public/images/products/${imageCategory}`);

            for (let i of images) {
                fs.unlink(`${dir}/${i}`, (err) => {
                    if (err) console.log(err);
                    console.log(`${i} was deleted`);
                });
            }
        }

        return Product.find({
            where: {
                id
            }
        }).then((product) => {
            if (files.length > 0) {
                files.forEach((image) => {
                    const {fieldname, filename} = image;
                    let imageName = `image-${uniqid.time()}.${filename.split('.').pop()}`;

                    if (fieldname === 'mainImage') {
                        fs.unlink(`${dir}/${product.mainImage}`, (err) => {
                            if (err) console.log(err);
                            console.log(`${product.mainImage} was deleted`);
                        });
                        dataToUpdate.mainImage = imageName;
                    }
                    image.pipe(fs.createWriteStream(`${dir}/${imageName}`));
                });
            }
            if (product) {
                return product.update(dataToUpdate, {
                    where: {
                        id
                    }
                }).then((updated) => {
                    return {
                        message: updated,
                        status: 200
                    }
                }).catch((e) => {
                    return {
                        message: e,
                        status: 400
                    };
                });
            }

            return {
                message: 'Товара нет в базе',
                status: 400
            };
        });
    }

    return {
        message: 'Я не вижу изменений товара',
        status: 400
    };
};
