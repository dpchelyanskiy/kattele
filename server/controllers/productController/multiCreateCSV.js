const {Product} = require('../../db/models/products');
const downloadIMG = require('../../controllers/CSVparser/IMGParser');

module.exports = multiCreateCSV = async (product) => {
    return Product.find({
        where: {
            article: product.article
        }
    }).then(async (exist) => {
        if (!exist) {
            let {file, destination} = await downloadIMG(product);
            product.mainImage = file;
            product.imageCategory = destination;

            return Product.create(product).then((created) => {
                return created
            })
        } else {
            return {
                _article: product.article
            }
        }
    }).catch((err) => {
        return err
    });
};
