const createOneProduct = require('./createOneProduct');
const getProductsList = require('./productsList');
const deleteProductById = require('./deleteProduct');
const viewOneProduct = require('./viewOneProduct');
const updateByID = require('./updateProduct');
const deleteMultiple = require('./multiDelete');
const multiCreateCSV = require('./multiCreateCSV');
const publicList = require('./publicList');
const topSalesList = require('./topSalesList');
const productsByCategory = require('./productsByCategory');
const publicCategory = require('./publicCategory');
const searchProducts = require('./searchPdoructs');

module.exports = class Product {
    createProduct(ctx) {
        return createOneProduct(ctx);
    };

    list(page, limit) {
        return getProductsList(page, limit);
    };

    deleteProduct(id) {
        return deleteProductById(id);
    };

    viewProduct(article, activeCurrency) {
        return viewOneProduct(article, activeCurrency);
    };

    updateProduct(req) {
        return updateByID(req);
    };

    bulkDelete(data) {
        return deleteMultiple(data);
    };

    bulkCSVCreate(data) {
        return multiCreateCSV(data);
    };

    publicList(activeCurrency) {
        return publicList(activeCurrency);
    };

    searchProducts(page, value, activeCurrency) {
        return searchProducts(page, value, activeCurrency);
    };

    topSalesList(activeCurrency) {
        return topSalesList(activeCurrency);
    }

    publicCategory(page, query, activeCurrency) {
        return publicCategory(page, query, activeCurrency);
    }

    productsByCategory(categoryName, page, query, activeCurrency) {
        return productsByCategory(categoryName, page, query, activeCurrency);
    }
};
