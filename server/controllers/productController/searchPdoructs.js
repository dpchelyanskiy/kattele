const {
    Sequelize,
    client
} = require('../../db/config/db');
const {Product} = require('../../db/models/products');
const {Currency} = require('../../db/models/currency');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const fs = require('fs');
const Favorites = require('../../controllers/favoritesController');

module.exports = (page, value, activeCurrency) => {
    const limit = 20;
    value = value.charAt(0).toUpperCase() + value.slice(1);
    return Product.findAndCountAll(
        {
            where: {
                name: {like: '%' + value + '%'}
                // category: {
                //     [client.Op.not]: null
                // },
                // published: true
            },
        }
    ).then((products, err) => {
        console.log(products);
        let pageCount = Math.ceil(products.count / limit),
            offset = (page * limit) - limit,
            productsCount = products.count;

        if (!err) {
            return Product.findAll({
                where: {
                    name: {like: '%' + value + '%'}
                    // category: {
                    //     [client.Op.not]: null
                    // },
                    // published: true
                },
                order: [['id', 'DESC']],
                offset: offset < 0 ? 0 : offset,
                limit: limit
            }).then(async (products) => {
                if (products.length === 0) {
                    return {
                        status: 200,
                        message: {
                            error: 'Ваш поиск не дал результатов',
                            status: 200
                        }
                    }
                }

                const favorList = await new Favorites().listOfFavorites();
                let favorListProducts = [];

                if(favorList.length) {
                    favorListProducts = favorList[0].dataValues.products;
                }

                const choosedCurrency = await Currency.findOne({where: {type: activeCurrency,}});

                for (let i in products) {
                    if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                        products[i].dataValues.sell_price /= choosedCurrency.rate;
                        products[i].dataValues.sell_price = products[i].dataValues.sell_price.toFixed(2);
                    }

                    for (let y in favorListProducts) {
                        if (Number(favorListProducts[y]) === (Number(products[i].id))) {
                            products[i].favorite = true;
                        }
                    }

                    // products[i].sell_price = products[i].sell_price * 2;
                    if (fs.existsSync(`${appDir}/public/images/products/${products[i].imageCategory})`)) {
                        const dir = path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`);
                        const files = fs.readdirSync(dir);

                        products[i].dataValues.allImages = files.filter((image) => image !== products[i].dataValues.image);
                    } else {
                        products[i].dataValues.allImages = ['Нет фотографий для слайдера']
                    }
                }

                return {
                    status: 200,
                    message: {
                        products,
                        pageCount,
                        productsCount,
                        productCurrency: choosedCurrency
                    }
                }
            }).catch((err) => {
                return {
                    status: 301,
                    message: err
                }
            })
        } else {
            return {
                status: 301,
                message: err
            }
        }
    });
};
