const {Product} = require('../../db/models/products');
const {client, Sequelize} = require('../../db/config/db');
const {Currency} = require('../../db/models/currency');

const path = require('path');
const appDir = path.dirname(require.main.filename);
const fs = require('fs');
const Favorites = require('../../controllers/favoritesController');

module.exports = topSalesList = (activeCurrency) => {
    const limit = 10;
    return Product.findAll({
        /* where: {
             category: {
                 [client.Op.not]: null
             },
             published: true
         },*/
        order: [
            [[Sequelize.fn('RANDOM')]]
        ],
        limit: limit
    }).then(async (products) => {
        if (products.length === 0) {
            return {
                status: 200,
                message: {
                    error: 'В категории нет товаров, пожалуйста выберите другую категорию товаров',
                    status: 200
                }
            }
        }

        const favorList = await new Favorites().listOfFavorites();
        let favorListProducts = [];

        if(favorList.length) {
            favorListProducts = favorList[0].dataValues.products;
        }

        const choosedCurrency = await Currency.findOne({where: {type: activeCurrency,}});

        for (let i in products) {
            if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                products[i].dataValues.sell_price /= choosedCurrency.rate;
                products[i].dataValues.sell_price = products[i].dataValues.sell_price.toFixed(2);
            }
            for (let y in favorListProducts) {
                if (Number(favorListProducts[y]) === (Number(products[i].id))) {
                    products[i].favorite = true;
                }
            }

            if (fs.existsSync(`${appDir}/public/images/products/${products[i].imageCategory})`)) {
                const dir = path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`);
                const files = fs.readdirSync(dir);

                products[i].dataValues.allImages = files.filter((image) => image !== products[i].dataValues.image);
            } else {
                products[i].dataValues.allImages = ['Нет фотографий для слайдера']
            }
        }

        return {
            status: 200,
            topList: {
                products,
                choosedCurrency
            }
        }
    }).catch((err) => {
        return {
            status: 301,
            message: err
        }
    });
};
