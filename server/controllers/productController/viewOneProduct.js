const {Product} = require('../../db/models/products');
const {Currency} = require('../../db/models/currency');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const fs = require('fs');

module.exports = viewOneProduct = (article, activeCurrency) => {
    return Product.findOne({
        where: {
            article
        }
    }).then(async (product) => {
        if (product) {
            const dir = path.resolve(`${appDir}/public/images/products/${product.imageCategory}`);
            const choosedCurrency = await Currency.findOne({where: {type: activeCurrency,}});

            if (choosedCurrency && choosedCurrency.type !== 'UAH') {
                product.dataValues.sell_price /= choosedCurrency.rate;
                product.dataValues.sell_price = product.dataValues.sell_price.toFixed(2);
                product.dataValues.trade_price /= choosedCurrency.rate;
                product.dataValues.trade_price = product.dataValues.trade_price.toFixed(2);
            }

            if (fs.existsSync(dir)) {
                const files = fs.readdirSync(dir);
                product.dataValues.allImages = files.filter((image) => image !== product.dataValues.mainImage);
            } else {
                product.dataValues.allImages = ['No image catalog'];
            }
            return {
                choosedCurrency,
                message: product,
                status: 200
            }
        }

        return {
            message: {
                error: 'Товара нет в базе',
            },
            status: 404
        }
    }).catch((err) => {
        return {
            message: err,
            status: 400
        }
    })
};
