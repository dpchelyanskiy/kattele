const {Product} = require('../../db/models/products');
const asyncBusboy = require('async-busboy');
const fs = require('fs');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const uniqid = require('uniqid');
const rimraf = require('rimraf');
const serialize = require('../helpers/serialize');

createProduct = async (ctx) => {
    const dir = path.resolve(`${appDir}/public/images/products/${uniqid.time('product-')}`);

    if (ctx.request.is('multipart/form-data')) {
        const {fields, files} = await asyncBusboy(ctx.req);
        const data = serialize(fields.data);

        const dirName = dir.split("products/").pop();

        if (files.length > 0) {
            const {article} = data;

            return Product.find({
                where: {
                    article
                },
                defaults: data
            }).then((product) => {
                if (!product) {
                    fs.mkdirSync(dir);

                    files.forEach((image) => {
                        const {fieldname, filename} = image;
                        let imageName = `image-${uniqid.time()}.${filename.split('.').pop()}`;
                        if (fieldname === 'mainImage') {
                            data.mainImage = imageName;
                        }
                        image.pipe(fs.createWriteStream(`${dir}/${imageName}`));
                    });

                    data.imageCategory = dirName;

                    return Product.create(data).then((newProduct) => {
                        return {
                            message: newProduct,
                            status: 200
                        }
                    })
                }

                return {
                    message: 'Продукт с таким Артикулом существует в базе',
                    status: 302
                }
            }).catch((e) => {
                rimraf(dir, () => {
                    console.log(`${dir} deleted`);
                });
                const errors = [];

                for (let i in e.errors) {
                    if (e.errors[i].type === 'notNull Violation') {
                        errors.push({
                            [e.errors[i].path]: 'не может быть пустым'
                        })
                    } else {
                        errors.push({
                            [e.errors[i].path]: e.errors[i].message
                        })
                    }
                }
                return {
                    message: errors,
                    status: 400
                };
            });
        } else {
            console.log('Here');

            return {
                message: 'Выберите фотографию торава',
                status: 400
            };
        }
    }
};

module.exports = createProduct;
