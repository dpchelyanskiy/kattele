const {Product} = require('../../db/models/products');
const path = require('path');
const rimraf = require('rimraf');
const appDir = path.dirname(require.main.filename);
const fs = require('fs');

module.exports = deleteMultiple = (id) => {
    return Product.findAll({
        where: {
            id
        }
    }).then((products) => {
        const existProduts = [];
        const deletedProductsTitles = [];

        for (let i in products) {
            existProduts.push(products[i].id);
            deletedProductsTitles.push(products[i].name);

            if (fs.existsSync(path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`))) {
                rimraf(path.resolve(`${appDir}/public/images/products/${products[i].imageCategory}`), () => {
                    console.log(`${products[i].imageCategory} deleted`);
                });
            }
        }

        return Product.destroy({
            where: {
                id: existProduts
            }
        }).then((res) => {
            return {
                message: `Товары ${deletedProductsTitles} удалены`,
                status: 200
            }
        }).catch((e) => {
            return {
                message: e,
                status: 404
            }
        });
    }).catch((e) => {
        return {
            message: 'В базе нет таких товаров',
            status: 404
        }
    });
};
