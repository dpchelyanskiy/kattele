const {Product} = require('../../db/models/products');
const fs = require('fs');
const json2csv = require('json2csv').parse;
const {katteleTemplate} = require('./templates/kattele');
const uniqid = require('uniqid');
const path = require('path');
const appDir = path.dirname(require.main.filename);
const {SITE_URL} = process.env;

module.exports = generateCSV = (data) => {
    return Product.findAll({
        where: {
            article: {
                in: data
            }
        },
        attributes: {
            exclude: ['createdAt', 'updatedAt', 'published', 'id']
        }
    }).then(async (products) => {
        if (products) {
            for (let i in products) {
                const {mainImage, imageCategory, size} = products[i].dataValues;
                for (let y in products[i].dataValues) {
                    products[i].mainImage = `${SITE_URL}/images/products/${imageCategory}/${mainImage}`;
                    if (katteleTemplate[y]) {
                        products[i].dataValues[katteleTemplate[y]] = products[i].dataValues[y];
                        products[i].supplier_name = 'Kattele';
                        products[i].supplier_city = 'Одесса';
                        products[i].size = size.join();
                    }
                }
            }

            const csvProduct = [];
            let sortedObject = {};
            products.forEach((item) => {
                for (let i in item.dataValues) {
                    if (katteleTemplate[i] === item[i]) {
                        sortedObject = {
                            ...sortedObject,
                            [i]: item.dataValues[i]
                        };
                    }
                }

                csvProduct.push(sortedObject);
            });

            const fields = ['Артикул', 'Наименование товара', 'Город поставщика', 'Имя поставщика', 'Цена товара', 'Размер', 'Наличие', 'Описание', 'Изображение товара', 'Ссылка на товар'];
            const opts = {fields};
            const csv = json2csv(csvProduct, opts);

            try {
                const dir = `${path.resolve(`${appDir}/public/csv/`)}`;
                const filename = `kattele-csv-${uniqid.time()}`;
                fs.writeFileSync(`${dir}/${filename}.csv`, csv);

                return {
                    message: {
                        csv_download_url: `/csv/${filename}`
                    },
                    status: 200
                }
            } catch (err) {
                return {
                    message: err,
                    status: 400
                };
            }
        }
    }).catch((err) => {
        return {
            message: err,
            status: 400
        };
    })
};
