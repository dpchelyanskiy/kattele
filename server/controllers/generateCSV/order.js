const {Order} = require('../../db/models/order');
const fs = require('fs');
const json2csv = require('json2csv').parse;
const {orderkatteleTemplate} = require('./templates/kattele');
const uniqid = require('uniqid');
const path = require('path');
const appDir = path.dirname(require.main.filename);

module.exports = generateOrderCSV = (data) => {
    return Order.findAll({
        // where: {
        //     article: {
        //         in: data
        //     }
        // },
        attributes: {
            exclude: ['createdAt', 'updatedAt', 'published', 'id']
        }
    }).then(async (orders) => {
        if (orders) {
            for (let i in orders) {
                for (let y in orders[i].dataValues) {
                    if (orderkatteleTemplate[y]) {
                        orders[i].dataValues[orderkatteleTemplate[y]] = orders[i].dataValues[y];
                    }
                }
            }

            const csvProduct = [];
            let sortedObject = {};
            orders.forEach((item) => {
                for (let i in item.dataValues) {
                    if (orderkatteleTemplate[i] === item[i]) {
                        sortedObject = {
                            ...sortedObject,
                            [i]: item.dataValues[i]
                        };
                    }
                }

                csvProduct.push(sortedObject);
            });

            const fields = ['Номер заказа', 'Пользователь', 'Почта', 'Телефон', 'Город', 'Страна', 'Номер отделениия', 'Комментариий', 'Сумма заказа', 'Валюта', 'Статус'];
            const opts = {fields};
            const csv = json2csv(csvProduct, opts);

            try {
                const dir = `${path.resolve(`${appDir}/public/csv/`)}`;
                const filename = `kattele-csv-order-${uniqid.time()}`;
                fs.writeFileSync(`${dir}/${filename}.csv`, csv);

                return {
                    message: {
                        csv_download_url: `/csv/${filename}`
                    },
                    status: 200
                }
            } catch (err) {
                return {
                    message: err,
                    status: 400
                };
            }
        }
    }).catch((err) => {
        return {
            message: err,
            status: 400
        };
    })
};
