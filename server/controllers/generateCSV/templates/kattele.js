const katteleTemplate = {
    "supplier_city": "Город поставщика",
    "supplier_name": "Имя поставщика",
    "article": "Артикул",
    "name": "Наименование товара",
    "sell_price": "Цена товара",
    "availability": "Наличие",
    "description": "Описание",
    "mainImage": "Изображение товара",
    "product_url": "Ссылка на товар",
    "size": "Размер"
};

const orderkatteleTemplate = {
    "orderID": "Номер заказа",
    "userName": "Пользователь",
    "userEmail": "Почта",
    "userPhone": "Телефон",
    "country": "Город",
    "city": "Страна",
    "branch_number": "Номер отделениия",
    "comment": "Комментариий",
    "totalPrice": "Сумма заказа",
    "currency": "Валюта",
    "status": "Статус",
};

module.exports = {
    katteleTemplate,
    orderkatteleTemplate
};
