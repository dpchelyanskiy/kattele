FROM node:10
WORKDIR /usr/src/kattele
COPY . .
COPY package*.json ./
RUN npm install
RUN npm install -g nodemon
EXPOSE 3000
ENV NAME kattele
CMD [ "npm", "run", "dev" ]
