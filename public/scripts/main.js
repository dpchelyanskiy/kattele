"use strict";

var _extends =
    Object.assign ||
    function (target) {
        for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i];
            for (var key in source) {
                if (Object.prototype.hasOwnProperty.call(source, key)) {
                    target[key] = source[key];
                }
            }
        }
        return target;
    };

function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
        });
    } else {
        obj[key] = value;
    }
    return obj;
}

$(function () {
    // CODE START
    function initMainPage() {
        new Swiper("#header-section .swiper-container", {
            // Optional parameters
            direction: "horizontal",
            loop: true,
            navigation: {
                nextEl: $("#header-section .swiper-button-next"),
                prevEl: $("#header-section .swiper-button-prev")
            },
            pagination: {
                el: "#header-section .swiper-pagination" // spaceBetween: 30,
                // clickable: true,
            }
        });
        new Swiper("#products-section .swiper-container", {
            // Optional parameters
            direction: "horizontal",
            loop: false,
            slidesPerView: 5,
            navigation: {
                nextEl: "#products-section .swiper-button-next",
                prevEl: "#products-section .swiper-button-prev"
            },
            spaceBetween: 10,
            breakpoints: {
                // when window width is <= 480px
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                // when window width is <= 640px
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30
                }
            }
        });
        new Swiper("#section-reviews .swiper-container", {
            // Optional parameters
            direction: "horizontal",
            loop: true,
            slidesPerView: 3,
            navigation: {
                nextEl: "#section-reviews .swiper-button-next",
                prevEl: "#section-reviews .swiper-button-prev"
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                // when window width is <= 640px
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30
                }
            },
            spaceBetween: 10
        }); ///hover container lang menu

        $(".lang-menu").hover(
            function () {
                $(this).addClass("cls-border-lang");
                $(this)
                    .children()
                    .eq(0)
                    .addClass("cls-borderbottom-lang");
                $(".lang-menu ul")
                    .stop()
                    .slideToggle(100);
            },
            function () {
                $(this).removeClass("cls-border-lang");
                $(this)
                    .children()
                    .eq(0)
                    .removeClass("cls-borderbottom-lang");
                $(".lang-menu ul")
                    .stop()
                    .slideToggle(100);
            }
        ); /// click languages

        $(".lang-menu ul li").on("click", function () {
            //select lang and apply changes
            var $lang = $(this).text();
            $(".lang-menu div").text($lang);
        }); // BRANDS

        var brandsList = $("#section-brands .brands-list");
        brandsList.css({
            height:
                brandsList
                    .find(".brand")
                    .first()
                    .height() *
                4 +
                "px"
        });
        $("#more-brands-box a").click(function () {
            brandsList.toggleClass("active");

            if (brandsList.hasClass("active")) {
                brandsList.css({
                    height: brandsList.prop("scrollHeight") + "px"
                });
            } else {
                brandsList.css({
                    height:
                        brandsList
                            .find(".brand")
                            .first()
                            .height() *
                        4 +
                        "px"
                });
            }
        }); // BRANDS END
    }

    function appInit() {
        $("#search-btn").click(function () {
            $("#search-container").addClass("active");
        });
        $("#search-container").click(function () {
            $(this).removeClass("active");
        });
    }

    function initCatalogPage() {
        var choosenPrice = $("#choosen-price");
        var maxPrice = 6666;
        var $catalogFiltersBlock = $("#catalog_filters_block");
        var $sortList = $("#catalog_filters_block .sort-list");
        var timer = void 0;

        $("#price-range").on("input", function (e) {
            e.preventDefault();
            var urlParams = new URLSearchParams(window.location.search);
            var sizeParam = urlParams.get("size")
                ? "size=" + urlParams.get("size")
                : "";
            var sorParams = urlParams.get("sortBy")
                ? "&sortBy=" + urlParams.get("sortBy")
                : "";

            var price = Math.round((maxPrice / 100) * +e.target.value);
            choosenPrice.html(price);
            var priceQuery = "&price=" + price;

            clearInterval(timer);
            timer = setTimeout(function () {
                if (price !== 0) {
                    var userUrl = window.location.pathname.split("/")[1];
                    var parentCategory = window.location.pathname.split("/")[2];
                    if (!isNaN(Number(parentCategory))) {
                        window.location.href =
                            "/" + userUrl + "/1?size=" + sizeParam + priceParam + sorParams;
                    } else {
                        window.location.href =
                            "/" +
                            userUrl +
                            "/" +
                            parentCategory +
                            "/1?" +
                            sizeParam +
                            priceQuery +
                            sorParams;
                    }
                } else {
                    var _userUrl = window.location.pathname.split("/")[1];
                    var _parentCategory = window.location.pathname.split("/")[2];
                    if (!isNaN(Number(_parentCategory))) {
                        window.location.href =
                            "/" + _userUrl + "/1?size=" + sizeParam + priceParam + sorParams;
                    } else {
                        window.location.href =
                            "/" +
                            _userUrl +
                            "/" +
                            _parentCategory +
                            "/1?" +
                            sizeParam +
                            sorParams;
                    }
                }
            }, 400);
        });

        new Swiper("#top-sales-section .swiper-container", {
            // Optional parameters
            direction: "horizontal",
            loop: true,
            slidesPerView: 4,
            navigation: {
                nextEl: "#top-sales-section .swiper-button-next",
                prevEl: "#top-sales-section .swiper-button-prev"
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 20
                },
                // when window width is <= 640px
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 30
                }
            },
            spaceBetween: 10
        });

        $catalogFiltersBlock.find("button").click(function () {
            $sortList.toggleClass("active");
        });

        $catalogFiltersBlock.find(".sort-list li").click(function () {
            var sortItem = $(this);
            sortItem
                .parent()
                .find("li")
                .removeClass("active");
            sortItem.addClass("active");
        });

        $("body").click(function (e) {
            var $target = $(e.target);

            if (!$target.closest("#catalog_filters_block").length) {
                $sortList.removeClass("active");
            }
        });
    }

    function initProductPage() {
        new Swiper("#product-page .all-images .swiper-container", {
            // Optional parameters
            direction: "vertical",
            loop: true,
            on: {
                slideChange: function slideChange() {
                    var currentImageSrc = $(
                        $("#product-page .all-images .swiper-container img").get(
                            this.activeIndex
                        )
                    ).attr("src");
                    $("#main-product-image").attr("src", currentImageSrc);
                },
                imagesReady: function imagesReady() {
                    var currentImageSrc = $(
                        $("#product-page .all-images .swiper-container img").get(
                            this.activeIndex
                        )
                    ).attr("src");
                    $("#main-product-image").attr("src", currentImageSrc);
                }
            },
            pagination: {},
            slidesPerView: 2,
            navigation: {
                nextEl: ".all-images .swiper-button-next",
                prevEl: ".all-images .swiper-button-prev"
            },
            breakpoints: {
                575: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                    direction: "horizontal",
                    pagination: {
                        el: "#product-page .all-images .swiper-pagination",
                        type: "bullets"
                    }
                }
            },
            spaceBetween: 10
        });
    }

    function initProductSizes() {
        var sizes = $(".product-info .sizes .size");
        sizes.click(function () {
            sizes.removeClass("active");
            $(this).addClass("active");
        });
    }

    $(document).ready(function () {
        appInit();
        initCart();
        var pageId = $("body .page").attr("id");

        switch (pageId) {
            case "main-page":
                initMainPage();
                break;

            case "catalog-page":
                initCatalogPage();
                break;

            case "product-page":
                initProductPage();
                initProductSizes();
                initCountBox();
                fastBuy();
                break;

            case "reset-password-page":
                initResetPasswordPage();
                break;

            case "registration-page":
                initRegistrationPage();
                break;

            case "login-page":
                initLoginPage();
                break;

            case "order-page":
                initOrderCountBox();
                initOrderPage();
                break;

            case "update-page":
                initUpdatePage();
                break;
            default:
                break;
        }
    });
});

// ON EMAIL RESET. Send message
function initRegistrationPage() {
    var $form = $("#registration-form");
    $form.on("submit", function (e) {
        e.preventDefault();
        var $emailInput = $($(this).find("#email-input"));
        var email = $emailInput.val();
        var userData = {};
        var formInputs = $('#registration-form input');
        for (let i = 0; i < $(formInputs).length; i++) {
            userData = {
                ...userData,
                [$(formInputs[i]).attr('name')]: $(formInputs[i]).val()
            }
        }
        userData = {
            ...userData,
            [$('#country-input').attr('name')]: $("#country-input option:selected").text()
        };

        if (!email || !checkEmail(email)) {
            return $emailInput.addClass("error");
        } else {
            $emailInput.removeClass("error");

            $.ajax({
                type: "POST",
                url: "/user/registration",
                data: userData,
                success: function success(data) {
                    console.log(data);
                    if (data.message.status === 'error') {
                        $('#registration-page .errorBlock').prepend("<div class='errorMessage'>" + data.message.msg + "</div>");
                    } else {
                        $("#successModal").modal("toggle");
                    }
                },
                error: function error(_error8) {
                    return _error8.responseText;
                }
            });
        }
    });
}

function initUpdatePage() {
    console.log('i am done');
    var $form = $("#update-form");
    $form.on("submit", function (e) {
        e.preventDefault();
        var $emailInput = $($(this).find("#email-input"));
        var email = $emailInput.val();
        var userData = {};
        var formInputs = $('#update-form input');

        for (let i = 0; i < $(formInputs).length; i++) {
            userData = {
                ...userData,
                [$(formInputs[i]).attr('name')]: $(formInputs[i]).val()
            }
        }
        userData = {
            ...userData,
            [$('#country-input').attr('name')]: $("#country-input option:selected").text()
        };

        if (!email || !checkEmail(email)) {
            return $emailInput.addClass("error");
        } else {
            $emailInput.removeClass("error");

            console.log(userData);
            $.ajax({
                type: "PUT",
                url: "/api/users/",
                data: userData,
                success: function success(data) {
                    console.log(data);
                    if ($('.errorMessage').length > 0) {
                        $('.errorMessage').remove();
                    }
                    if (data.status === 'error') {
                        $('#update-page .errorBlock').prepend("<div class='errorMessage'>" + data.msg + "</div>");
                    } else {
                        $("#successModal").modal("toggle");
                    }
                },
                error: function error(_error8) {
                    if ($('.errorMessage').length > 0) {
                        $('.errorMessage').remove();
                    }
                    if (_error8.responseJSON.msg.name === "SequelizeDatabaseError") {
                        $('#update-page .errorBlock').prepend("<div class='errorMessage'> Не првильно введен номер телефона </div>");
                    }
                    return _error8.responseText;
                }
            });
        }
    });
}

function initLoginPage() {
    var $form = $("#login-form");
    $form.on("submit", function (e) {
        e.preventDefault();
        var $emailInput = $($(this).find("#email-input"));
        var email = $emailInput.val();

        if (!email || !checkEmail(email)) {
            return $emailInput.addClass("error");
        } else {
            var user = {
                email: email,
                password: $("#password").val()
            };
            $.ajax({
                type: "POST",
                url: "/user/login",
                data: user,
                success: function success(data) {
                    $('.errorMessage').remove();
                    if (data.status === 404) {
                        $('#login-page .error-messages').prepend("<div class='errorMessage'>" + data.message + "</div>");
                    } else {
                        window.location.href = "/";
                    }
                },
                error: function error(err) {
                    console.log(err);
                    $(".error-messages").html(err.responseJSON.message);
                }
            });
            $emailInput.removeClass("error");
        }
    });
}

function initResetPasswordPage() {
    $("#reset-password-form").on("submit", function (e) {
        e.preventDefault();

        var $emailInput = $(this).find("#email-input");
        var email = $emailInput.val();

        if (!email || !checkEmail(email)) {
            return $emailInput.addClass("error");
        } else {
            $emailInput.removeClass("error");

            $.ajax({
                type: "POST",
                url: "/user/password-reset",
                data: {email},
                success: function success(data) {
                    if (data.status === 'error') {
                        $('#reset-password-page .errorBlock').prepend("<div class='errorMessage'>" + data.msg + "</div>");
                    } else {
                        $("#sendMessageModal").modal("toggle");
                    }
                },
                error: function error(_error8) {
                    return _error8.responseText;
                }
            });
        }
    });
}

function initCountBox() {
    var $countBox = $(".count-box");
    var $prev = $countBox.find(".count-box__button_prev");
    var $next = $countBox.find(".count-box__button_next");
    $(".count-box__count-input").val(1);
    $(".count-box__count-input").change(function () {
        var $input = $(this);
        var val = $input.val();

        if (isNaN(val)) {
            return $input.val(1);
        }

        if (val.length > 4) {
            val = val.substring(0, 4);
        }

        if (val <= 0) {
            val = val * -1;
        }

        $input.val(val);
    });
    $prev.click(function () {
        var $input = $(
            $(this)
                .parent()
                .find(".count-box__count-input")
                .get(0)
        );
        var val = $input.val();

        if (+val > 1) {
            $input.val(+val - 1);
        }
    });
    $next.click(function () {
        var $input = $(
            $(this)
                .parent()
                .find(".count-box__count-input")
                .get(0)
        );
        $input.val(+$input.val() + 1);
    }); // CODE END
}

$(".sort-list li").click(function (e) {
    e.preventDefault();
    var urlParams = new URLSearchParams(window.location.search);
    var sizeParam = urlParams.get("size") ? "size=" + urlParams.get("size") : "";
    var priceParam = urlParams.get("price")
        ? "&price=" + urlParams.get("price")
        : "";
    var sorParams = e.target.innerText;

    window.location.search = "" + sizeParam + priceParam + "&sortBy=" + sorParams;
});

$(".sub-filters-box label").click(function (e) {
    e.preventDefault();
    var urlParams = new URLSearchParams(window.location.search);
    var sizeParam = urlParams.get("size");
    var priceParam = urlParams.get("price")
        ? "&price=" + urlParams.get("price")
        : "";
    var sorParams = urlParams.get("sortBy")
        ? "&sortBy=" + urlParams.get("sortBy")
        : "";

    if (sizeParam !== null) {
        var queryParams = sizeParam.split(",");
        var searchID = e.target.innerText;

        if ($.inArray(searchID, queryParams) !== -1) {
            queryParams.splice($.inArray(searchID, queryParams), 1);
        } else {
            queryParams.push(searchID);
        }

        if (queryParams.length === 0) {
            window.location.search = "" + queryParams + priceParam + sorParams;
        } else {
            var userUrl = window.location.pathname.split("/")[1];
            var parentCategory = "/" + window.location.pathname.split("/")[2];
            window.location.href =
                "/" +
                userUrl +
                parentCategory +
                "/1?size=" +
                queryParams +
                priceParam +
                sorParams;
        }
    } else {
        var sizeQuery = [];
        sizeQuery.push(e.target.innerText);
        var _userUrl2 = window.location.pathname.split("/")[1];
        var parentCategory = window.location.pathname.split("/")[2];
        if (!isNaN(Number(parentCategory))) {
            window.location.href =
                "/" + _userUrl2 + "/1?size=" + sizeQuery + priceParam + sorParams;
        } else {
            window.location.href =
                "/" +
                _userUrl2 +
                "/" +
                parentCategory +
                "/1?size=" +
                sizeQuery +
                priceParam +
                sorParams;
        }
    }
});

$(".pagination li a").click(function (e) {
    e.preventDefault();
    var urlQuewry = window.location.search;

    if ($(e.target).hasClass("nextpage")) {
        var URI = window.location.href.split("/?")[0];
        var page = Number($(".pagination li a.active").html());
        page++;

        window.location.href = "/" + URI.split("/")[3] + "/" + (page + urlQuewry);
    } else if ($(e.target).hasClass("prevpage")) {
        var _page = Number($(".pagination li a.active").html());
        var _URI = window.location.href.split("/?")[0];

        if (_page !== 1) {
            _page--;
            window.location.href =
                "/" + _URI.split("/")[3] + "/" + (_page + urlQuewry);
        }
    } else {
        var userUrl = window.location.pathname.split("/")[1];
        var parentCategory = window.location.pathname.split("/")[2];
        if (!isNaN(Number(parentCategory))) {
            window.location.href =
                "/" + userUrl + "/" + ($(e.target).html() + urlQuewry);
        } else {
            window.location.href =
                "/" +
                userUrl +
                "/" +
                parentCategory +
                "/" +
                ($(e.target).html() + urlQuewry);
        }
    }
});

var urlParams = new URLSearchParams(window.location.search);
var sizeParam = urlParams.get("size");
var priceParam = urlParams.get("price");
var sorParams = urlParams.get("sortBy");

if (sizeParam || priceParam || sorParams) {
    var canselFilterButton = $("#catalog-page .catalog-header .my-btn--blue");
    $(canselFilterButton).addClass("active");

    $(canselFilterButton).click(function (e) {
        window.location.search = "";
    });
}

if (sizeParam !== null) {
    sizeParam.split(",").forEach(function (item) {
        $("[data-id=" + item + "]").attr("checked", true);
    });
}

if (priceParam !== null) {
    $("#choosen-price").html(priceParam);
    var maxPrice = 6666;
    var price = Math.round((Number(priceParam) / maxPrice) * 100);

    $(".form-control-range").val(price);
}

if (sorParams !== null) {
    var listItem = $(".sort-list li");
    for (var i = 0; i < $(listItem).length; i++) {
        $($(listItem)[i]).removeClass("active");
        if ($($(listItem)[i]).html() === sorParams) {
            $($(listItem)[i]).addClass("active");
        }
    }
}

function checkEmail(email) {
    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    return pattern.test(email);
}

$(".buy-product").click(function (e) {
    var productData = {
        id: Number($(".product-info-container").attr("id")),
        count: Number($(".count-box__count-input").val()),
        name: $(".text-uppercase").html(),
        image: $("#main-product-image").attr("src"),
        article: $(".productArticle").html()
    };
    if ($(".size.active").length > 0) {
        productData.size = Number($(".size.active").html());
    }

    sendToCart(productData);
    checkSize();
    removeError();
});

function initCart() {
    $.ajax({
        type: "GET",
        url: "/user/cart",
        success: function success(data) {
            if (data.cart.products.length > 0) {
                var totalPrice = data.totalPrice;
                var _data$cart = data.cart,
                    products = _data$cart.products,
                    choosedCurrency = _data$cart.choosedCurrency;

                var cart = $(".bucket__products .scroll-box");
                $(cart).empty();

                if ($("#order-page").length > 0) {
                    popupCartTemplate(cart, products, choosedCurrency);
                    orderPageCart(products, choosedCurrency);
                } else {
                    popupCartTemplate(cart, products, choosedCurrency);
                }

                $(".purchase-sum").html(totalPrice + " " + choosedCurrency.ru_lang);
                return false;
            }
            emptyCart();
        },
        error: function error(_error) {
            return {
                message: _error.statusText,
                status: _error.status
            };
        }
    });
}

function popupCartTemplate(cart, products, currency) {
    console.log(currency.ru_lang);
    for (var _i in products) {
        $(cart).append(
            '<div class="product" data-id="' +
            products[_i].product.cartID +
            '">\n                <div class="img-box">\n                    <img alt="" src="../../images/products/' +
            products[_i].product.imageCategory +
            "/" +
            products[_i].product.mainImage +
            '">\n                </div>\n                \n                <div class="info-box">\n                    <div class="row">\n                        <div class="col-12">\n                            <p class="title">' +
            products[_i].product.name +
            '</p>\n                        </div>\n                    </div>\n                    \n                    <div class="row">\n                        <p class="size col-6">\u0420\u0430\u0437\u043C\u0435\u0440 - ' +
            products[_i].product.size +
            '</p>\n                        <p class="count col-6">\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E - ' +
            products[_i].product.count +
            '</p>\n                    </div>\n\n                    <div class="row price-box">\n                        <div class="col-12">\n                            <p>\u0418\u0442\u043E\u0433\u043E: ' +
            products[_i].item_total_price +
            " " +
            currency.ru_lang +
            '</p>\n                        </div>\n                    </div>\n                </div>\n                <button class="product__remove icon-link icon-link__chrest" type="button" onclick="removeCartItem(event)"></button>\n            </div>'
        );
    }
}

function sendToCart(productData) {
    if (productData.size > 0) {
        $.ajax({
            type: "POST",
            url: "/user/cart",
            data: productData,
            success: function success(data) {
                updateCart(data, productData);
            },
            error: function error(_error2) {
                return {
                    message: _error2.statusText,
                    status: _error2.status
                };
            }
        });
    }
}

function removeCartItem(e) {
    var product = $(e.target).offsetParent(),
        productID = $(product).data("id"),
        productData = {
            id: productID
        };

    $.ajax({
        type: "DELETE",
        url: "/user/cart",
        data: productData,
        success: function success(data) {
            updateCart(data);
            $(".emptyCart").remove();

            if (Object.keys(data.cart.items).length === 0) {
                emptyCart();
            }
        },
        error: function error(_error3) {
            console.log(_error3);
            return _error3;
        }
    });
}

function updateCart(data, productData) {
    var _data$cart2 = data.cart,
        items = _data$cart2.items,
        totalPrice = _data$cart2.totalPrice;

    if (productData !== undefined) {
        $.amaran({
            theme: "user blue",
            content: {
                img: productData.image,
                user: productData.name,
                message:
                    productData.article +
                    " </br> \u0422\u043E\u0432\u0430\u0440 \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D \u0432 \u043A\u043E\u0440\u0437\u0438\u043D\u0443"
            },

            stickyButton: true,
            resetTimeout: true,
            closeOnClick: false,
            position: "top right",
            outEffect: "slideBottom"
        });
    }

    var cart = $(".bucket__products .scroll-box");
    $(cart).empty();

    var ru_lang = data.choosedCurrency.ru_lang;

    for (var _i2 in items) {
        $(cart).append(
            '<div class="product" data-id="' +
            _i2 +
            '">\n                    <div class="img-box">\n                            <img alt="" src="../../images/products/' +
            items[_i2].product.imageCategory +
            "/" +
            items[_i2].product.mainImage +
            '">\n                    </div>\n                    <div class="info-box">\n                        <div class="row">\n                            <div class="col-12">\n                                <p class="title">' +
            items[_i2].product.name +
            '</p>\n                            </div>\n                        </div>\n                        <div class="row">\n                            <p class="size col-6">\u0420\u0430\u0437\u043C\u0435\u0440 - ' +
            items[_i2].product.size +
            '</p>\n                            <p class="count col-6">\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E - ' +
            items[_i2].product.count +
            '</p>\n                        </div>\n                        <div class="row price-box">\n                            <div class="col-12">\n                                <p>\u0418\u0442\u043E\u0433\u043E: ' +
            items[_i2].item_total_price +
            " " +
            ru_lang +
            '</p>\n                            </div>\n                        </div>\n                    </div>\n                    <button class="product__remove icon-link icon-link__chrest" type="button" onclick="removeCartItem(event)"></button>\n                </div>'
        );
    }

    $(".purchase-sum").html(totalPrice + " " + ru_lang);
}

function emptyCart() {
    $(".bucket__products .scroll-box").append(
        '<div class="emptyCart">В корзине пусто :(</div>'
    );
}

function removeError() {
    for (var _i3 = 0; _i3 < $(".fast-buy-form input").length; _i3++) {
        $($(".fast-buy-form input")[_i3]).removeClass("error");
    }
}

function fastBuy() {
    $(".fast-buy-form .fast-buy-button").on("click", function () {
        var form = $(this).parents(".fast-buy-form");
        var produtSize = $(".size.active");

        var fastOrder = {
            productID: $(".product-info-container").attr("id"),
            productCount: $(".count-box__count-input").val(),
            productSize: $(produtSize).length > 0 ? $(produtSize).html() : ""
        };

        checkSize();

        for (var _i4 = 0; _i4 < $(form).find("input").length; _i4++) {
            fastOrder = _extends(
                {},
                fastOrder,
                _defineProperty(
                    {},
                    $($(form).find("input")[_i4]).attr("id"),
                    $($(form).find("input")[_i4]).val()
                )
            );
        }

        fastOrderCheck(fastOrder);
    });
}

function fastOrderCheck(data) {
    !checkEmail(data.email) ? (data.email = "") : data.email;

    for (var _i5 in data) {
        var target = "[data-target=" + _i5 + "]";
        $(target).addClass("error");
        if (data[_i5].length > 0) {
            $(target).removeClass("error");
        } else if (
            data.email.length > 0 &&
            data.phone.length > 0 &&
            data.productSize.length > 0
        ) {
            data[_i5] = data[_i5].replace(/[.*+?^${}()\s-|[\]\\]/g, "");
            $.ajax({
                type: "POST",
                url: "/user/fast-order",
                data: data,
                success: function success(data) {
                    $('.count-box__count-input').val('1');
                },
                error: function error(_error4) {
                    return _error4.responseText;
                }
            });
            for (var _i6 = 0; _i6 < $(".sizes .size").length; _i6++) {
                $(".sizes .size.active").removeClass("active");
            }
            clearInput();
        }
    }
}

function clearInput() {
    var inputCount = $(".fast-buy-form input").length;
    for (var _i7 = 0; _i7 < inputCount; _i7++) {
        $(".fast-buy-form input")[_i7].value = "";
    }
}

function checkSize() {
    for (var _i8 = 0; _i8 < $(".sizes .size").length; _i8++) {
        $(".sizes .size").css("border-color", "#000");

        if ($(".sizes .size.active").length === 0) {
            $(".sizes .size").css("border-color", "red");
        }
    }
}

function setMask(phone) {
    $(phone).mask("+38Z (00) 000-00-00", {
        translation: {
            Z: {
                pattern: /[0]/g,
                fallback: "0"
            }
        }
    });
}

$('[data-target="phone"]').on("keyup", function () {
    setMask(this);
});

$(".product__image-box").click(function (e) {
    var targetID = $($(this).offsetParent()).data("id");
    var productData = {
        image: $($(this).offsetParent())
            .find(".card-img-top")
            .attr("src"),
        name: $($(this).offsetParent())
            .find(".product__title")
            .html(),
        message:
            "\u0422\u043E\u0432\u0430\u0440 \u0434\u043E\u0431\u0430\u0432\u043B\u0435\u043D \u0432 \u0438\u0437\u0431\u0440\u0430\u043D\u043D\u043E\u0435"
    };

    if ($(e.target).hasClass("product__like")) {
        e.preventDefault();
        var likeButton = $(this).find(".product__like");

        var setWishListData = {
            id: !!targetID ? targetID : ""
        };

        $.ajax({
            type: "POST",
            url: "/user/wishes",
            data: setWishListData,
            success: function success(data) {
                var active =
                    data.favor.status === 200 ? $(likeButton).hasClass("active") : false;

                if (active) {
                    $(likeButton).removeClass("active");

                    productData.message = "Товар удален из избранного";
                } else {
                    $(likeButton).addClass("active");
                }

                if ($("#section_choosed_products").length > 0) {
                    $("[data-id=" + setWishListData.id + "]").remove();
                    $(".products-block .product").length === 0
                        ? $(".products-block").append(
                        '<div class="noProducts">Нет избранных товаров :(</div>'
                        )
                        : "";
                }

                $.amaran({
                    theme: "user blue",
                    content: {
                        img: productData.image,
                        user: productData.name,
                        message: productData.message
                    },
                    stickyButton: true,
                    resetTimeout: true,
                    closeOnClick: false,
                    position: "top right",
                    outEffect: "slideBottom"
                });
            },
            error: function error(_error5) {
                return {
                    message: _error5.statusText,
                    status: _error5.status
                };
            }
        });
    }
});

$(".action-btn__exit").click(function () {
    $.ajax({
        type: "POST",
        url: "/user/logout",
        success: function success() {
            window.location.href = "/";
        },
        error: function error(err) {
            console.log(err);
        }
    });
});

function initOrderPage() {
    countOrderPrice();
}

function initOrderCountBox() {
    var $countBox = $(".count-box");
    var $prev = $countBox.find(".count-box__button_prev");
    var $next = $countBox.find(".count-box__button_next");
    $(".count-box__count-input").val(1);
    $(".count-box__count-input").change(function () {
        var $input = $(this);
        var val = $input.val();

        if (isNaN(val)) {
            return $input.val(1);
        }

        if (val.length > 4) {
            val = val.substring(0, 4);
        }

        if (val <= 0) {
            val = val * -1;
        }

        $input.val(val);
        countOrderPrice();
    });

    $prev.click(function () {
        var $input = $(
            $(this)
                .parent()
                .find(".count-box__count-input")
                .get(0)
        );
        var val = $input.val();

        if (+val > 1) {
            $input.val(+val - 1);
            countOrderPrice();
        }
    });

    $next.click(function () {
        var $input = $(
            $(this)
                .parent()
                .find(".count-box__count-input")
                .get(0)
        );
        $input.val(+$input.val() + 1);
        countOrderPrice();
    }); // CODE END
}

function countOrderPrice() {
    var $products = $(".order-section .product");
    var $orderPrice = $("#order-price");
    var orderPrice = 0;

    $products.each(function (key, product) {
        var $product = $(product);

        var priceVal = +$($product.find(".product-price").get(0)).html() || 0;
        var countVal = +$($product.find(".product-count").get(0)).val() || 0;
        var fullPrice = $($product.find(".full-product-price").get(0));

        var price = +priceVal * +countVal;

        orderPrice += price;
        fullPrice.html(price);
    });

    $orderPrice.html(orderPrice);

    // $('#order-form').submit(handleOrder)
}

// async function handleOrder(e) {
//     e.preventDefault();
//     const $products = $('.order-section .product');
//     console.log('products: ', $products);
//     const {
//         city,
//         country,
//         email,
//         name,
//         phone,
//         branch_number,
//         comment
//     } = getFormData(this);
//
//     if (!email || !checkEmail(email)) {
//         return $(this).find('input[name=email]').addClass('error');
//     } else {
//         $(this).find('input[name=email]').removeClass('');
//     }
//
//     try {
//         const response = $.post('/url', {
//             city,
//             country,
//             email,
//             name,
//             phone,
//             branch_number,
//             comment
//         });
//
//         if (response.status === '200') {
//             // SHOW SUCCESS MESSAGE
//         }
//     } catch (e) {
//         // SHOW ERROR MESSAGE
//     }
// }
//
// function getFormData(form) {
//     return $(form).serializeArray().reduce(function (obj, item) {
//         obj[item.name] = item.value;
//         return obj;
//     }, {})
// }

function orderPageCart(products, choosedCurrency) {
    var cart = $("#order-page .bucket__products .scroll-box");
    $(cart).empty();
    for (var _i9 in products) {
        $(cart).append(
            '<div class="product" data-id="' +
            products[_i9].product.cartID +
            '">\n                <div class="row">\n                <div class="img-box col p-0">\n                    <img alt="" src="../../images/products/' +
            products[_i9].product.imageCategory +
            "/" +
            products[_i9].product.mainImage +
            '">\n                </div>\n                \n                <div class="info-box col p-0">\n                    <div class="row">\n                        <div class="col-12">\n                            <p class="title">' +
            products[_i9].product.name +
            '</p>\n                        </div>\n                    </div>\n                   \n                    <div class="row p-0 price-box">\n                        <div class="col-12">\n                            <p><span class="product-price">' +
            products[_i9].product.sell_price +
            "</span> " +
            choosedCurrency.ru_lang +
            '</p>\n                        </div>\n                    </div>\n\n                    <div class="row">\n                        <div class="col-12">\n                            <p class="size">\u0420\u0430\u0437\u043C\u0435\u0440 - <span class="size-number">' +
            products[_i9].product.size +
            '</span></p>\n                        </div>\n                    </div>\n                    \n                    <div class="row">\n                        <div class="col-12">\n                            <h5 class="product-info__title">\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E: </h5>\n                            <div class="count-box">\n                                <button class="count-box__button count-box__button_prev">-</button>\n                                <input type="number" class="count-box__count-input product-count" maxlength="99" value="' +
            products[_i9].product.count +
            '" disabled/>\n                                <button class="count-box__button count-box__button_next">+</button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                \n                <div class="row price-box col-12 p-0 mt-2">\n                    <div class="col-12">\n                        <p>\u0418\u0442\u043E\u0433\u043E: <span class="full-product-price">' +
            products[_i9].item_total_price +
            "</span> " +
            choosedCurrency.ru_lang +
            '</p>\n                    </div>\n                </div>\n                <button type="button" class="product__remove icon-link icon-link__chrest" onclick="removeCartItem(event)"></button>    \n                </div>           \n            </div>'
        );
    }

    cartCounter();
}

function cartCounter() {
    var cartItem = {
        id: "",
        action: ""
    };
    $(".count-box__button").click(function (e) {
        cartItem.id = $(this)
            .parents(".product")
            .data("id");
        if ($(this).hasClass("count-box__button_prev")) {
            cartItem.action = "minus";
        } else {
            cartItem.action = "plus";
        }

        $.ajax({
            type: "POST",
            url: "/user/cart-counter",
            data: cartItem,
            success: function success(data) {
                var price = $.find(
                    "#order-page [data-id=" + cartItem.id + "] .full-product-price"
                );
                var quantity = $.find(
                    "#order-page [data-id=" + cartItem.id + "] .product-count"
                );
                $(price).html(data.cart.items[cartItem.id].item_total_price);
                $(quantity).val(data.cart.items[cartItem.id].product.count);
                var cart = $(".modal .bucket__products .scroll-box");
                $(cart).empty();
                popupCartTemplate(cart, data.cart.items, data.choosedCurrency);

                $(".purchase-sum").html(data.cart.totalPrice + ' ' + data.choosedCurrency.ru_lang);
            },
            error: function error(_error6) {
                console.log(_error6);
                return _error6;
            }
        });
    });
}

$("#order-form .ibtn").click(function (e) {
    e.preventDefault();
    var form = $(this).offsetParent(),
        input = $(form).find("input"),
        orderUserData = {},
        emailHandler = $("input[name=email]"),
        validEmail = checkEmail($(emailHandler).val()),
        choosedCountry = $("#country-input option:selected").text(),
        comment = $("[name=comment]").val();

    for (var _i10 = 0; _i10 < $(input).length; _i10++) {
        var _extends3;

        $(input[_i10]).removeClass("error");
        orderUserData.canSend = true;
        if ($(input[_i10]).val().length === 0) {
            $(input[_i10]).addClass("error");
            orderUserData.canSend = false;
        }

        if (!validEmail) {
            $(emailHandler).addClass("error");
            orderUserData = _extends({}, orderUserData, {
                email: "",
                canSend: false
            });
        }

        orderUserData = _extends(
            {},
            orderUserData,
            ((_extends3 = {
                country: choosedCountry
            }),
                _defineProperty(
                    _extends3,
                    $(input[_i10]).attr("name"),
                    $(input[_i10]).val()
                ),
                _defineProperty(_extends3, "comment", comment),
                _extends3)
        );
    }

    makeOrder(orderUserData);
});

function makeOrder(orderUserData) {
    if (orderUserData.canSend) {
        $.ajax({
            type: "POST",
            url: "/user/make-order",
            data: orderUserData,
            success: function success(data) {
                var cart = $(".scroll-box");
                $(cart).empty();
                $("#order-page .purchase-sum").empty();
                emptyCart();
            },
            error: function error(_error7) {
                return _error7.responseText;
            }
        });
    }
}

function changeCurrency(e) {
    var choosedCurrency = $(e.target)[0].innerText;

    $.ajax({
        type: "POST",
        url: "/user/change-currency",
        data: {currency: choosedCurrency},
        success: function success(data) {
            window.location.reload();
        },
        error: function error(_error8) {
            return _error8.responseText;
        }
    });
}


$('.main-search-field').on('keyup', function (e) {
    e.preventDefault();
    if (event.keyCode === 13) {
        window.location.href = "/catalog-search/1/" + $(e.target).val()
    }
});
